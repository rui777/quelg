# Quelg
Quelg is a library of language-integrated query based on the tagless-final approach.

Available from http://logic.cs.tsukuba.ac.jp/~rui/quelg

Disclaimer: This software is a work-in-progress research prototype,
with some rough edges and features which may not work as expected.
Use of this software is at your sole risk. 

## Build

### Prerequisites
OCaml, version at least 4.0

### Build instructions

Run `make`. 

```
make
```

If all goes well, you should have following libraries.

- quel.cma ... Main library 
- schema.cma ... The schema definition as an example 

## Running instructions in the OCaml REPL

To get started, open a terminal window from the directory there are libraries and type `ocaml`.
You'll then be in the OCaml REPL(Real Event Print Loop).

```
OCaml version 4.07.1
#
```

To then include libraries simply load it.

```
#load "quel.cma";;
#load "schema.cma";;
```


If you want to be able to refer to the contents of the module without this explicit qualification, open `Schema` module.

```
open Schema;;
```

Quelg term is expressed by combinators based on the tagless final approach. The syntax of Quelg and its typing rules are defined as `Symantics` interface in `quelg_sym.mli`.

Now, we can write a simple query, such as:

```
module Example(S:SYM_SCHEMA) = struct
  open S
  let orders = table ("orders", orders ())
  let key = oid
  let alpha = [(qty, (string "sum"), (string "qty_sum"))]
  let q = observe @@ fun () ->
    group key alpha (foreach (fun () -> orders) @@ fun o ->
    yield o)
       (fun v key -> q_res key (qty_sum v))
end ;;
(*
  module Example :
  functor (S : Schema.SYM_SCHEMA) ->
    sig
      val orders : < oid : int; pid : int; qty : int > list S.repr
      val key : < oid : int; pid : int; qty : int > S.repr -> int S.repr
      val alpha :
        ((< oid : int; pid : int; qty : int > S.repr -> int S.repr) *
         string S.repr * string S.repr)
        list
      val q : < key : int; qty_sum : int > list S.obs
    end
*)
```

(The output of the interpreter is shown in comments.)
The name `S` will be used for an instance of `SYM_SCHEMA`.
`SYM_SCHEMA` is a sigunature of combining `Symantics` and the schema.
`Schema` module defines the schema as an example. The type of the query is `<key:int; qty_sum:int> repr`. The type `'a repr` represent the Quelg's type. The function `observe` is observe the `'a repr` as a value of some observation type `'a obs`, which is also kept abstract. OCaml's `@@`, like Haskel's `$`, is the low-precedence infix operator for applications.


We stand for _interpreter_, an instance of `SYM_SCHEMA` or `Symantics`.
We can evaluate the example using the R interpreter.
We write a new print function to display the result of the R interpreter.

```
let rec print_qty_sum = function
  | [] -> ""
  | (o::os) -> "<key=" ^ (string_of_int o#key) ^
               ", average=" ^ (string_of_int o#qty_sum) ^
               ">; " ^ print_qty_sum os ;;

let module M = Example(R) in M.q;;
(*  - : string = "<key=1, average=5>; <key=2, average=30>; <key=3, average=20>; " *)
```

We can also evaluate the example by the P interpreter which interprets every term to a string. Note, the type `'a P.obs` is `string`.

```
let module M = Example(P) in M.q;;
(*
  group (fun x -> (x.oid); <key=(x.oid);qty_sum=sum(x.qty)>)
  (foreach (fun () -> table "orders") (fun y -> yield y))
  - : unit = ()    
*)
```

We can evaluate the above to translate an SQL string by the `GenSQL` interpreter.
In our implementation, the queries for grouping are transformed as subqueries for clarity.
```
let module M = Example(FixGenSQL) in M.q ;;
(*
  - : < key : int; qty_sum : int > list Schema.FixGenSQL.obs =
  "SELECT x.oid AS key, SUM(x.qty) AS qty_sum 
   FROM (SELECT y.* 
        FROM orders AS y WHERE true) AS x 
   WHERE true 
   GROUP BY x.oid"
#
*)
```

## Examples
There are an example of query normalization in `example_q1.ml` ~ `example_q9.ml`.

## Files
Here is an inventory of the source code in the Quelg directory.

- quelg_sym.mli: Define Symantics interface as the syntax and its type
- quelg_r.ml: The R interpreter
- quelg_p.ml: The pretty-printer
- quelg_sql.ml: The SQL translator
- quelg_o.ml: The optimization framework including the Trans(bwd/fwd) signature
- quelg_norm.ml: Normalization rules based on the framework
- quelg_fix.ml: The recursive module to iterate normalizations
- schema.ml: Define the schema as an example
- schema.sql: Define SQL queries to setup example schema and data
- example_q1.ml: Examples of Q1
- example_q2.ml: Examples of Q2
- example_q3.ml: Examples of Q3
- example_q4.ml: Examples of Q4
- example_q5.ml: Examples of Q5
- example_q6.ml: Examples of Q6
- example_q7.ml: Examples of Q7
- example_q8.ml: Examples of Q8
- example_q9.ml: Examples of Q9
- lnil.ml: Nil-surpression rule

## Caveats
For brevity, current version of Quelg does not access a database server.
GenSQL module generate a real SQL string.
You can use this string to access a database.
If you need database access, you can use the PostgreSQL-OCaml library
that take a SQL string to acccess databases.

   Postgresql-ocaml: http://mmottl.github.io/postgresql-ocaml/
