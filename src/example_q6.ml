(* 
 * Examples of Q_6
 * Query with Predicate + group is used in query's input
 *
 * Q_6' = λp. for(g <- Q_1)
 *             where p(g.sale_sum)
 *             yield g
 *
 * Q_6 = Q_6' (λx. x > 10000)
 *
 *)


#load "quel.cma";;
#load "schema.cma";;

open Schema
open Printf

module Q1(S:SYM_SCHEMA) = struct
  open S
  let table_orders   = table ("orders", orders ())
  let table_products = table ("products", products ())

  let key = ooid
  let alpha = [(osale, (string "sum"), (string "sale_sum"))]
  let query = foreach (fun () -> table_products) @@ fun p ->
              foreach (fun () -> table_orders) @@ fun o ->
              where ((pid p) =% (opid o)) @@ fun () ->
              yield @@ osales (oid o) ((price p) *% (qty o))

  let q1 = group key alpha query @@ fun v key -> q1_res key (sale_sum v)

  let observe = observe
end

module Q6'(S:SYM_SCHEMA) = struct
  open S
  module M = Q1(S)
  let q6' = fun p ->
       foreach (fun () -> M.q1) @@ fun g ->
       where (p (q1_sale_sum g)) @@ fun () ->
       yield g

  let observe = observe
end

module Q6(S:SYM_SCHEMA) = struct
  open S
  module M = Q6'(S)
  let q6 = M.q6' (fun x -> x >% (int 10000))

  let observe = observe
end

(* Auxiliary printers *)
let rec print_sale_sum = function
  | [] -> ""
  | (o::os) -> "<key=" ^ (string_of_int o#key) ^
               ", sale_sum=" ^ (string_of_int o#sale_sum) ^
               ">; " ^ print_sale_sum os ;;

let module M = Q6(R) in
print_sale_sum @@ M.observe (fun () -> M.q6) ;;

let module M = Q6(FixP) in
print_endline @@ M.observe (fun () -> M.q6) ;;

let module M = Q6(FixGenSQL) in M.observe (fun () -> M.q6) ;;

(* Performance test *)
let avg l = (List.fold_left (+.) 0.0 l) /. float(List.length l) ;;

(* Normalize + SQL generation time *)
let r =
  let module M = Q6(FixGenSQL) in
  let perform () : float =
    let s = Sys.time () in
    ignore (M.observe (fun () -> M.q6));
    Sys.time () -. s
  in let rec loop (data:float list) = function
    | 0 -> data
    | n -> loop ((perform ())::data) (n-1)
  in loop [] 100
in avg r ;;
