(* 
 * Examples of Q_4
 * Query with lambda expression
 *
 * Q_4' = λoid. G(oid,α)(for(o <- table("orders"))
 *                         where(o.oid = oid)
 *                         yield o)
 *   Where α = {qty, AVG, qty_avg}
 * 
 * Q_4 = Q_4' 2
 * 
 *)


#load "quel.cma";;
#load "schema.cma";;

open Schema
open Printf

module Q4'(S:SYM_SCHEMA) = struct
  open S
  let table_orders = table ("orders", orders ())
                   
  let key = oid
  let alpha = [(qty, (string "avg"), (string "qty_avg"))]
  let q4' = lam (fun xoid ->
       group key alpha
       (foreach (fun () -> table_orders) @@ fun o ->
        where ((oid o) =% xoid) @@ fun () ->
        yield o) @@ fun v key -> q4_res key (qty_avg v))
end
 
module Q4(S:SYM_SCHEMA) = struct
  open S
  module M = Q4'(S)
  let q4 = app M.q4' (int 2)
 
  let observe = observe
end

(* Auxiliary printers *)
let rec print_qty_avg = function
  | [] -> ""
  | (o::os) -> "<key=" ^ (string_of_int o#key) ^
               ", qty_avg=" ^ (string_of_int o#qty_avg) ^
               ">; " ^ print_qty_avg os ;;

let module M = Q4(R) in
print_qty_avg @@ M.observe (fun () -> M.q4) ;;

let module M = Q4(FixP) in
print_endline @@ M.observe (fun () -> M.q4) ;;

let module M = Q4(FixGenSQL) in M.observe (fun () -> M.q4) ;;

(* Performance test *)
let avg l = (List.fold_left (+.) 0.0 l) /. float(List.length l) ;;

(* Normalize + SQL generation time *)
let r =
  let module M = Q4(FixGenSQL) in
  let perform () : float =
    let s = Sys.time () in
    ignore (M.observe (fun () -> M.q4));
    Sys.time () -. s
  in let rec loop (data:float list) = function
    | 0 -> data
    | n -> loop ((perform ())::data) (n-1)
  in loop [] 100
in avg r ;;
