(*
#load "quel.cma";;
*)

open Quelg_sym
open Quelg_r
open Quelg_p
open Quelg_sql
open Quelg_o
open Quelg_norm
open Format

(* An example schema definition as a symantics interface *)
module type SCHEMA = sig
  type 'a repr

  (* input / output records *)
  val product : int repr -> string repr -> int repr ->
                <pid:int; name:string; price:int> repr

  val order   : int repr -> int repr -> int repr ->
                <oid:int; pid:int; qty:int> repr

  val sales   : int repr -> string repr -> int repr ->
                <pid:int; name:string; sale:int> repr

  val q_res     : int repr -> int repr -> 
                  <key:int; qty_sum:int> repr

  val q1_res    : int repr -> int repr -> 
                  <key:int; sale_sum:int> repr

  val q2'_res   : int repr -> int repr -> int repr ->
                  <key:int; sale_sum:int; qty_sum:int> repr

  val q2_res    : int repr -> int repr ->
                  <key:int; average:int> repr

  val q4_res    : int repr -> int repr -> 
                  <key:int; qty_avg:int> repr

  val q5_res    : int repr -> int repr -> 
                  <key:int; qty_count:int> repr

  val q7_res    : int repr -> int repr ->
                  <key:int; count_count:int> repr

  val q8_res    : int repr -> int repr ->
                  <key:int; sale_count:int> repr

  val q9_res    : string repr -> int repr ->
                  <key:string; score_avg:int> repr

  val osales    : int repr -> int repr -> <oid:int; sale:int> repr

  val qsales    : int repr -> int repr -> int repr -> <oid:int; sale:int; qty:int> repr

  val classes : string repr -> <cclass:string> repr

  val students : string repr -> string repr -> <cclass:string; name:string> repr

  val tests : string repr -> string repr -> int repr -> <name:string; course:string; score:int> repr

  val test : string repr -> int repr -> <name:string; score:int> repr

  val student_res : string repr -> int repr -> <key:string; score_avg:int> repr

  val nested_res : string repr -> <key:string; score_avg:int> list repr -> 
                       <cclass:string; student:<key:string; score_avg:int> list> repr

  val scores : string repr -> int repr -> <cclass:string; score:int> repr

  (* projection *)
  val pid      : <pid:int; name:string; price:int> repr -> int repr
  val name     : <pid:int; name:string; price:int> repr -> string repr
  val price    : <pid:int; name:string; price:int> repr -> int repr

  val oid      : <oid:int; pid:int; qty:int> repr -> int repr
  val opid     : <oid:int; pid:int; qty:int> repr -> int repr
  val qty      : <oid:int; pid:int; qty:int> repr -> int repr

  val spid     : <pid:int; name:string; sale:int> repr -> int repr
  val sname    : <pid:int; name:string; sale:int> repr -> string repr
  val sale     : <pid:int; name:string; sale:int> repr -> int repr

  val ooid      : <oid:int; sale:int> repr -> int repr
  val osale     : <oid:int; sale:int> repr -> int repr

  val qoid      : <oid:int; sale:int; qty:int> repr -> int repr
  val qsale     : <oid:int; sale:int; qty:int> repr -> int repr
  val qqty      : <oid:int; sale:int; qty:int> repr -> int repr

  val cclass  : <cclass:string> repr -> string repr

  val stclass  : <cclass:string; name:string> repr -> string repr
  val stname   : <cclass:string; name:string> repr -> string repr

  val tname   : <name:string; course:string; score:int> repr -> string repr
  val tcourse : <name:string; course:string; score:int> repr -> string repr
  val tscore  : <name:string; course:string; score:int> repr -> int repr

  val test_name  : <name:string; score:int> repr -> string repr
  val test_score : <name:string; score:int> repr -> int repr

  val nested_class   : <cclass:string; student:<key:string; score_avg:int> list> repr -> string repr
  val nested_student : <cclass:string; student:<key:string; score_avg:int> list> repr ->
                       <key:string; score_avg:int> list repr

  val scores_class : <cclass:string; score:int> repr -> string repr
  val scores_score : <cclass:string; score:int> repr -> int repr

  val sale_sum    : <field:string; value:int> list repr -> int repr
  val qty_avg     : <field:string; value:int> list repr -> int repr
  val qty_count   : <field:string; value:int> list repr -> int repr
  val count_count : <field:string; value:int> list repr -> int repr
  val sale_count  : <field:string; value:int> list repr -> int repr 
  val qty_sum     : <field:string; value:int> list repr -> int repr
  val score_avg   : <field:string; value:int> list repr -> int repr

  val q1_sale_sum    : <key:int; sale_sum:int> repr -> int repr
  val q2_key         : <key:int;sale_sum:int;qty_sum:int> repr -> int repr
  val q2_sale_sum    : <key:int;sale_sum:int;qty_sum:int> repr -> int repr
  val q2_qty_sum     : <key:int;sale_sum:int;qty_sum:int> repr -> int repr
  val q3_sale_sum    : <key:int; sale_sum:int> repr -> int repr
  val q5_qty_count   : <key:int; qty_count:int> repr -> int repr
  val q7_count_count : <key:int; count_count:int> repr -> int repr
  val q8_sale_count  : <key:int; sale_count:int> repr -> int repr
  val q9_score_avg   : <key:string; score_avg:int> repr -> int repr 

  (* data sources *)
  val products       : unit -> <pid:int; name:string; price:int> list
  val orders         : unit -> <oid:int; pid:int; qty:int> list
  val table_classes  : unit -> <cclass:string> list
  val table_students : unit -> <cclass:string; name:string> list
  val table_tests    : unit -> <name:string; course:string; score:int> list

end

(* composition of Symantics and SCHEMA *)
module type SYM_SCHEMA = sig
  include SymanticsL
  include SCHEMA  with type 'a repr := 'a repr
end

(* the R interpreter with the schema *)
module R_schema = struct
  type 'a repr = 'a

  let product pid name price =
    object
      method pid  = pid
      method name = name
      method price = price
    end

  let order oid pid qty =
    object
      method oid = oid
      method pid = pid
      method qty = qty
    end

  let sales pid name sale =
    object
      method pid  = pid
      method name = name
      method sale = sale
    end

  let q_res key qty_sum =
    object
      method key      = key
      method qty_sum  = qty_sum
    end

  let q1_res key sale_sum =
    object
      method key       = key
      method sale_sum = sale_sum
    end

  let q2'_res key sale_sum qty_sum = 
    object
      method key = key
      method sale_sum = sale_sum
      method qty_sum = qty_sum
    end

  let q2_res key average = 
    object
      method key = key
      method average = average
    end

  let q4_res key qty_avg =
    object
      method key     = key
      method qty_avg = qty_avg
    end

  let q5_res key qty_count =
    object
      method key       = key
      method qty_count = qty_count
    end

  let q7_res key count_count =
    object
      method key         = key
      method count_count = count_count
    end

  let q8_res key sale_count = 
    object
      method key = key
      method sale_count = sale_count
    end

  let q9_res key score_avg =
    object
      method key = key
      method score_avg = score_avg
    end

  let osales oid sale =
    object
      method oid  = oid
      method sale = sale
    end

  let qsales oid sale qty =
    object
      method oid  = oid
      method sale = sale
      method qty  = qty
    end

  let classes cclass = 
    object
      method cclass = cclass
    end

  let students cclass name =
    object
      method cclass = cclass
      method name = name
    end

  let tests name course score =
    object
      method name = name
      method course = course
      method score = score
    end

  let test name score =
    object
      method name = name
      method score = score
    end

  let student_res key score_avg =
    object
      method key = key
      method score_avg = score_avg
    end

  let nested_res cclass student =
    object
      method cclass = cclass
      method student = student
    end

  let scores cclass score =
    object
      method cclass = cclass
      method score = score
    end

  (* projection *)
  let pid  o     = o#pid
  let name o     = o#name
  let price o    = o#price

  let oid o      = o#oid
  let opid o     = o#pid
  let qty o      = o#qty

  let spid o     = o#pid
  let sname o    = o#name
  let sale o     = o#sale

  let ooid o   = o#oid
  let osale o  = o#sale

  let qoid o   = o#oid
  let qsale o  = o#sale
  let qqty o   = o#qty

  let cclass o = o#cclass

  let stclass o = o#cclass
  let stname o  = o#name

  let tname o   = o#name
  let tcourse o = o#course
  let tscore o  = o#score

  let test_name o  = o#name
  let test_score o = o#score

  let nested_class o   = o#cclass
  let nested_student o = o#student

  let scores_class o = o#cclass
  let scores_score o = o#score

  let rec output ofield = function 
      record::tl -> if ((record#field) = ofield) then record 
              else (output ofield tl)
    | [] -> failwith "Output field does not exist. "

  let sale_sum o    = (output "sale_sum" o)#value
  let qty_avg o     = (output "qty_avg" o)#value
  let qty_count o   = (output "qty_count" o)#value
  let count_count o = (output "count_count" o)#value
  let sale_count o  = (output "sale_count" o)#value
  let qty_sum o     = (output "qty_sum" o)#value
  let score_avg o   = (output "score_avg" o)#value

  let q1_sale_sum o    = o#sale_sum
  let q2_key o         = o#key
  let q2_sale_sum o    = o#sale_sum
  let q2_qty_sum o     = o#qty_sum
  let q3_sale_sum o    = o#sale_sum
  let q5_qty_count o   = o#qty_count
  let q7_count_count o = o#count_count
  let q8_sale_count o  = o#sale_count
  let q9_score_avg o   = o#score_avg

  (* data sources *)
  let products () =
    [ product 110 "shirt"  100;
      product 111 "T-shirt"  200;
      product 210 "pants" 500;
      product 310 "suit"  1000;
    ]

  let orders () =
    [ order 1 110 2;
      order 1 111 3;
      order 2 110 5;
      order 2 210 10;
      order 2 310 15;
      order 3 310 20;
    ]

  let table_classes () = 
    [ classes "A";
      classes "B";
    ]
  let table_students () =
    [ students "A" "Jackson";
      students "B" "Noah";
      students "A" "Sophia";
      students "B" "Olivia";
    ]
  let table_tests () =
    [ tests "Jackson" "Math" 80;
      tests "Jackson" "Science" 75;
      tests "Noah" "Math" 59;
      tests "Noah" "Physics" 55;
      tests "Sophia" "Math" 92;
      tests "Sophia" "Science" 84;
      tests "Olivia" "Biology" 67;
      tests "Olivia" "Math" 65;
    ]

end

(* Compose the RL interpreter and the schema
   For simplicity, we redefine the name of `R'
*)
module R = struct
  include RL
  include (R_schema : SCHEMA
           with type 'a repr := 'a repr)
end

(* the P interpreter with the schema *)
module P = struct
  include PL

  (* records *)
  let product pid name price = fun p v ppf ->
    fprintf ppf "@[<2><pid=%t;name=%t;price=%t>@]"
      (pid p v) (name p v) (price p v)

  let order oid pid qty = fun p v ppf ->
    fprintf ppf "@[<2><oid=%t;pid=%t;qty=%t>@]"
      (oid p v) (pid p v) (qty p v)

  let sales pid name sale = fun p v ppf ->
    fprintf ppf "@[<2><pid=%t;name=%t;sale=%t>@]"
      (pid p v) (name p v) (sale p v)

  let osales oid sale = fun p v ppf ->
    fprintf ppf "@[<2><pid=%t;sales=%t>@]"
      (oid p v) (sale p v)

  let qsales oid sale qty = fun p v ppf ->
    fprintf ppf "@[<2><pid=%t;sale=%t;qty=%t>@]"
      (oid p v) (sale p v) (qty p v)

  let q_res key qty_sum = fun p v ppf ->
    fprintf ppf "@[<2><key=%t;qty_sum=%t>@]"
      (key p v) (qty_sum p v)

  let q1_res key sales_sum = fun p v ppf ->
    fprintf ppf "@[<2><key=%t;sales_sum=%t>@]"
      (key p v) (sales_sum p v)

  let q2'_res key sales_sum qty_sum = fun p v ppf ->
    fprintf ppf "@[<2><key=%t;sales_sum=%t;qty_sum=%t>@]"
      (key p v) (sales_sum p v) (qty_sum p v)

  let q2_res key average = fun p v ppf ->
    fprintf ppf "@[<2><key=%t;average=%t>@]"
      (key p v) (average p v)

  let q4_res key qty_avg = fun p v ppf ->
    fprintf ppf "@[<2><key=%t;qty_avg=%t>@]"
      (key p v) (qty_avg p v)

  let q5_res key qty_count = fun p v ppf ->
    fprintf ppf "@[<2><key=%t;qty_count=%t>@]"
      (key p v) (qty_count p v)

  let q7_res key count_count = fun p v ppf ->
    fprintf ppf "@[<2><key=%t;count_count=%t>@]"
      (key p v) (count_count p v)

  let q8_res key sale_count = fun p v ppf ->
    fprintf ppf "@[<2><key=%t;sale_count=%t>@]"
      (key p v) (sale_count p v)

  let q9_res key score_avg = fun p v ppf ->
    fprintf ppf "@[<2><key=%t;score_avg=%t>@]"
      (key p v) (score_avg p v)

  let classes cclass = fun p v ppf ->
    fprintf ppf "@[<2><class=%t>@]"
      (cclass p v)

  let students cclass name = fun p v ppf ->
    fprintf ppf "@[<2><class=%t;name=%t>@]"
      (cclass p v) (name p v)

  let tests name course score = fun p v ppf ->
    fprintf ppf "@[<2><name=%t;course=%t;score=%t>@]"
    (name p v) (course p v) (score p v)

  let test name score = fun p v ppf ->
    fprintf ppf "@[<2><name=%t;score=%t>@]"
    (name p v) (score p v)

  let student_res key score_avg = fun p v ppf ->
    fprintf ppf "@[<2><key=%t;score_avg=%t>@]"
    (key p v) (score_avg p v)

  let nested_res cclass student = fun p v ppf ->
    fprintf ppf "@[<2><name=%t;student=%t>@]"
    (cclass p v) (student p v)

  let scores cclass score = fun p v ppf ->
    fprintf ppf "@[<2><class=%t;score=%t>@]"
    (cclass p v) (score p v)

  (* projection *)
  let print_projection r field_name paren = fun p v ppf ->
    paren ppf (p > 10) @@
    fun ppf -> fprintf ppf "@[<2>%t.%s@]" (r p v) field_name

  let pid r      = print_projection r "pid" paren
  let name r     = print_projection r "name" paren
  let price r    = print_projection r "price" paren

  let oid r   = print_projection r "oid" paren
  let opid r  = print_projection r "pid" paren
  let qty r   = print_projection r "qty" paren

  let spid r  = print_projection r "pid" paren
  let sname r = print_projection r "name" paren
  let sale r  = print_projection r "sale" paren

  let ooid r   = print_projection r "oid" paren
  let osale r  = print_projection r "sale" paren

  let qoid r   = print_projection r "oid" paren
  let qsale r  = print_projection r "sale" paren
  let qqty r   = print_projection r "qty" paren

  let cclass r = print_projection r "class" paren

  let stclass r = print_projection r "class" paren
  let stname r  = print_projection r "name" paren

  let tname r   = print_projection r "name" paren
  let tcourse r = print_projection r "course" paren
  let tscore r  = print_projection r "score" paren

  let test_name r  = print_projection r "name" paren
  let test_score r = print_projection r "score" paren

  let nested_class r   = print_projection r "class" paren
  let nested_student r = print_projection r "student" paren

  let scores_class r = print_projection r "class" paren
  let scores_score r = print_projection r "score" paren

  let sale_sum r    = print_projection r "sale" paren_sum
  let qty_avg r     = print_projection r "qty" paren_avg
  let qty_count r   = print_projection r "qty" paren_count
  let count_count r = print_projection r "qty_count" paren_count
  let sale_count r  = print_projection r "sale_sum" paren_count
  let qty_sum r     = print_projection r "qty" paren_sum
  let score_avg r   = print_projection r "score" paren_avg

  let q1_sale_sum r    = print_projection r "sale_sum" paren
  let q2_key r         = print_projection r "key" paren
  let q2_sale_sum r    = print_projection r "sale_sum" paren
  let q2_qty_sum r     = print_projection r "qty_sum" paren
  let q3_sale_sum r    = print_projection r "sale_sum" paren
  let q5_qty_count r   = print_projection r "qty_count" paren
  let q7_count_count r = print_projection r "count_count" paren
  let q8_sale_count r  = print_projection r "sale_count" paren
  let q9_score_avg r   = print_projection r "score_avg" paren

  (* data sources, it not use in printing *)
  let products       = fun () -> []
  let orders         = fun () -> []
  let table_classes  = fun () -> []
  let table_students = fun () -> []
  let table_tests    = fun () -> []

end


(* the SQL translator with the schema *)
module GenSQL = struct
  include GenSQL

  (* records *)
  let product pid name price =
    record @@ ("pid" %: pid) %* (row1 ("name" %: name))
  let order oid pid qty =
    record @@ ("oid" %: oid) %* ("pid" %: pid) %* (row1 ("qty" %: qty))
  let sales pid name sale =
    record @@ ("pid" %: pid) %* ("name" %: name) %* (row1 ("sale" %: sale))

  let q_res key qty_sum =
    record @@ key %* (row1 ("qty_sum" %: qty_sum))

  let q1_res key sale_sum =
    record @@ key %* (row1 ("sale_sum" %: sale_sum))

  let q2'_res key sale_sum qty_sum =
    record @@ key %* ("sale_sum" %: sale_sum) %* (row1 ("qty_sum" %: qty_sum))

  let q2_res key average =
    record @@ ("key" %: key) %* (row1 ("average" %: average))

  let q4_res key qty_avg =
    record @@ key %* (row1 ("qty_avg" %: qty_avg))

  let q5_res key qty_count =
    record @@ key %* (row1 ("qty_count" %: qty_count))

  let q7_res key count_count =
    record @@ key %* (row1 ("count_count" %: count_count))

  let q8_res key sale_count = 
    record @@ key %* (row1 ("sale_count" %: sale_count))

  let q9_res key score_avg = 
    record @@ key %* (row1 ("score_avg" %: score_avg))

  let osales oid sale =
    record @@ ("oid" %: oid) %* (row1 ("sale" %: sale))

  let qsales oid sale qty =
    record @@ ("oid" %: oid) %* ("sale" %: sale) %* (row1 ("qty" %: qty))

  let classes cclass =
    record @@ (row1 ("class" %: cclass))

  let students cclass name =
    record @@ ("class" %: cclass) %* (row1 ("name" %: name))

  let tests name course score =
    record @@ ("name" %: name) %* ("course" %: course) %* (row1 ("score" %: score))

  let test name score =
    record @@ ("name" %: name) %* (row1 ("score" %: score))

  let student_res key score_avg =
    record @@ key %* (row1 ("score_avg" %: score_avg))

  let nested_res cclass student =
    record @@ ("class" %: cclass) %* (row1 ("student" %: student))

  let scores cclass score = 
    record @@ ("class" %: cclass) %* (row1 ("score" %: score))

  (* projection *)
  let pid r      = r %. "pid"
  let name r     = r %. "name"
  let price r    = r %. "price"

  let oid r   = r %. "oid"
  let opid r  = r %. "pid"
  let qty r   = r %. "qty"

  let spid r  = r %. "pid"
  let sname r = r %. "name"
  let sale r  = r %. "sale"

  let ooid r   = r %. "oid"
  let osale r  = r %. "sale"

  let qoid r   = r %. "oid"
  let qsale r  = r %. "sale"
  let qqty r   = r %. "qty"

  let cclass r = r %. "class"

  let stclass r = r %. "class"
  let stname r  = r %. "name"

  let tname r   = r %. "name"
  let tcourse r = r %. "course"
  let tscore r  = r %. "score"

  let test_name r  = r %. "name"
  let test_score r = r %. "score"

  let nested_class r   = r %. "class"
  let nested_student r = r %. "student"

  let scores_class r = r %. "class"
  let scores_score r = r %. "score"

  let sale_sum r    = sum_ (r %. "sale")
  let qty_avg r     = avg_ (r %. "qty")
  let qty_count r   = count_ (r %. "qty")
  let count_count r = count_ (r %. "qty_count")
  let sale_count r  = count_ (r %. "sale_sum")
  let qty_sum r     = sum_ (r %. "qty")
  let score_avg r   = avg_ (r %. "score")

  let q1_sale_sum r    = r %. "sale_sum"
  let q2_key r         = r %. "key"
  let q2_sale_sum r    = r %. "sale_sum"
  let q2_qty_sum r     = r %. "qty_sum"
  let q3_sale_sum r    = r %. "sale_sum"
  let q5_qty_count r   = r %. "qty_count"
  let q7_count_count r = r %. "count_count"
  let q8_sale_count r  = r %. "sale_count"
  let q9_score_avg r   = r %. "score_avg"

  (* data sources, it not use in the SQL translation *)
  let products = fun () -> []
  let orders   = fun () -> []
  let table_classes = fun () -> []
  let table_students = fun () -> []
  let table_tests = fun () -> []

end

(* the optimizer with the schema *)

(* The default optimizer for the schema *)
module O_schema(X:Trans)(S:SYM_SCHEMA with type 'a repr = 'a X.from) = struct
  open X
  open S

    (* records *)
  let product pid name price =
    fwd @@ S.product (bwd pid) (bwd name) (bwd price)

  let order oid pid qty =
    fwd @@ S.order (bwd oid) (bwd pid) (bwd qty)

  let sales pid name sale =
    fwd @@ S.sales (bwd pid) (bwd name) (bwd sale)

  let q_res key qty_sum =
    fwd @@ S.q_res (bwd key) (bwd qty_sum)

  let q1_res key sale_sum =
    fwd @@ S.q1_res (bwd key) (bwd sale_sum)

  let q2'_res key sale_sum qty_sum =
    fwd @@ S.q2'_res (bwd key) (bwd sale_sum) (bwd qty_sum)

  let q2_res key average =
    fwd @@ S.q2_res (bwd key) (bwd average)

  let q4_res key qty_avg =
    fwd @@ S.q4_res (bwd key) (bwd qty_avg)

  let q5_res key qty_count =
    fwd @@ S.q5_res (bwd key) (bwd qty_count)

  let q7_res key count_count =
    fwd @@ S.q7_res (bwd key) (bwd count_count)

  let q8_res key sale_count = 
    fwd @@ S.q8_res (bwd key) (bwd sale_count)

  let q9_res key score_avg = 
    fwd @@ S.q9_res (bwd key) (bwd score_avg)

  let osales oid sale =
    fwd @@ S.osales (bwd oid) (bwd sale)

  let qsales oid sale qty =
    fwd @@ S.qsales (bwd oid) (bwd sale) (bwd qty)

  let classes cclass = 
    fwd @@ S.classes (bwd cclass)

  let students cclass name = 
    fwd @@ S.students (bwd cclass) (bwd name)

  let tests name course score = 
    fwd @@ S.tests (bwd name) (bwd course) (bwd score)

  let test name score = 
    fwd @@ S.test (bwd name) (bwd score)

  let student_res key score_avg = 
    fwd @@ S.student_res (bwd key) (bwd score_avg)

  let nested_res cclass student = 
    fwd @@ S.nested_res (bwd cclass) (bwd student)

  let scores cclass score = 
    fwd @@ S.scores (bwd cclass) (bwd score)

  (* projection *)
  let pid      = fun o -> fwd @@ S.pid (bwd o)
  let name     = fun o -> fwd @@ S.name (bwd o)
  let price    = fun o -> fwd @@ S.price (bwd o)

  let oid      = fun o -> fmap S.oid o
  let opid     = fun o -> fmap S.opid o
  let qty      = fun o -> fmap S.qty o

  let spid     = fun o -> fwd @@ S.spid (bwd o)
  let sname    = fun o -> fwd @@ S.sname (bwd o)
  let sale     = fun o -> fwd @@ S.sale (bwd o)

  let ooid     = fun o -> fmap S.ooid o
  let osale    = fun o -> fmap S.osale o

  let qoid     = fun o -> fmap S.qoid o
  let qsale    = fun o -> fmap S.qsale o
  let qqty     = fun o -> fmap S.qqty o

  let cclass   = fun o -> fmap S.cclass o

  let stclass  = fun o -> fmap S.stclass o
  let stname   = fun o -> fmap S.stname o

  let tname    = fun o -> fmap S.tname o
  let tcourse  = fun o -> fmap S.tcourse o
  let tscore   = fun o -> fmap S.tscore o

  let test_name  = fun o -> fmap S.test_name o
  let test_score = fun o -> fmap S.test_score o

  let nested_class   = fun o -> fmap S.nested_class o
  let nested_student = fun o -> fmap S.nested_student o

  let scores_class = fun o -> fmap S.scores_class o
  let scores_score = fun o -> fmap S.scores_score o

  let sale_sum       = fun o -> fmap S.sale_sum o
  let qty_avg        = fun o -> fmap S.qty_avg o
  let qty_count      = fun o -> fmap S.qty_count o
  let count_count    = fun o -> fmap S.count_count o
  let sale_count     = fun o -> fmap S.sale_count o
  let qty_sum        = fun o -> fmap S.qty_sum o
  let score_avg      = fun o -> fmap S.score_avg o

  let q1_sale_sum    = fun o -> fmap S.q1_sale_sum o
  let q2_key         = fun o -> fmap S.q2_key o
  let q2_sale_sum    = fun o -> fmap S.q2_sale_sum o
  let q2_qty_sum     = fun o -> fmap S.q2_qty_sum o
  let q3_sale_sum    = fun o -> fmap S.q3_sale_sum o
  let q5_qty_count   = fun o -> fmap S.q5_qty_count o
  let q7_count_count = fun o -> fmap S.q7_count_count o
  let q8_sale_count  = fun o -> fmap S.q8_sale_count o
  let q9_score_avg   = fun o -> fmap S.q9_score_avg o

  (* data sources; these are not used in optimizations *)
  let products = S.products
  let orders   = S.orders
  let table_classes = S.table_classes
  let table_students = S.table_students
  let table_tests = S.table_tests

end

module O
    (X:Trans)
    (S:SYM_SCHEMA with type 'a repr = 'a X.from) = struct
  include OL(X)(S)
  include (O_schema(X)(S): SCHEMA
           with type 'a repr := 'a repr)

end


(* Optimization rules with the schema *)

(* Compose normalizations and the schema *)
module AbsBeta(F:SYM_SCHEMA) = struct
  module M = AbsBeta_pass(F)
  include O(M.X)(F)
  include M.IDelta
end

(* beta-reduction for record *)
module RecordBeta_pass(F:SYM_SCHEMA) = struct
  module X0 = struct
    type 'a from = 'a F.repr
    type 'a term =
      | Unknown : 'a from -> 'a term
      | Product : (int term * string term * int term) ->
        <pid:int;name:string;price:int> term
      | Order   : (int term * int term * int term) ->
        <oid:int;pid:int;qty:int> term
      | Sales   : (int term * string term * int term) ->
        <pid:int; name:string; sale:int> term
      | Q_res  : (int term * int term) ->
        <key:int;qty_sum:int> term
      | Q1_res  : (int term * int term) ->
        <key:int;sale_sum:int> term
      | Q2'_res  : (int term * int term * int term) ->
        <key:int;sale_sum:int;qty_sum:int> term
      | Q2_res  : (int term * int term) ->
        <key:int;average:int> term
      | Q4_res  : (int term * int term) ->
        <key:int;qty_avg:int> term
      | Q5_res  : (int term * int term) ->
        <key:int;qty_count:int> term
      | Q7_res  : (int term * int term) ->
        <key:int;count_count:int> term
      | Q8_res  : (int term * int term) ->
        <key:int;sale_count:int> term
      | Q9_res  : (string term * int term) ->
        <key:string;score_avg:int> term
      | Osales  : (int term * int term) ->
        <oid:int;sale:int> term
      | Qsales  : (int term * int term * int term) ->
        <oid:int;sale:int;qty:int> term
      | Classes : (string term) ->
        <cclass:string> term
      | Students : (string term * string term) ->
        <cclass:string; name:string> term
      | Tests : (string term * string term * int term) ->
        <name:string; course:string; score:int> term
      | Test : (string term * int term) ->
        <name:string; score:int> term
      | Student_res : (string term * int term) ->
        <key:string; score_avg:int> term
      | Nested_res : (string term * <key:string; score_avg:int> list term) ->
        <cclass:string; student:<key:string; score_avg:int> list> term
      | Scores : (string term * int term) ->
        <cclass:string; score:int> term
    let fwd x = Unknown x
    let rec bwd : type a. a term -> a from = function
      | Unknown x  -> x
      | Product(pid,name,price) ->
        F.product (bwd pid) (bwd name) (bwd price)
      | Order(oid,pid,qty) ->
        F.order (bwd oid) (bwd pid) (bwd qty)
      | Sales(pid,name,sale) ->
        F.sales (bwd pid) (bwd name) (bwd sale)
      | Q_res(key,qty_sum) ->
        F.q_res (bwd key) (bwd qty_sum)
      | Q1_res(key,sale_sum) ->
        F.q1_res (bwd key) (bwd sale_sum)
      | Q2'_res(key,sale_sum,qty_sum) ->
        F.q2'_res (bwd key) (bwd sale_sum) (bwd qty_sum)
      | Q2_res(key,average) ->
        F.q2_res (bwd key) (bwd average)
      | Q4_res(key,qty_avg) ->
        F.q4_res (bwd key) (bwd qty_avg)
      | Q5_res(key,qty_count) ->
        F.q5_res (bwd key) (bwd qty_count)
      | Q7_res(key,count_count) ->
        F.q7_res (bwd key) (bwd count_count)
      | Q8_res(key,sale_count) ->
        F.q8_res (bwd key) (bwd sale_count)
      | Q9_res(key,score_avg) ->
        F.q9_res (bwd key) (bwd score_avg)
      | Osales(oid,sale) ->
        F.osales (bwd oid) (bwd sale)
      | Qsales(oid,sale,qty) ->
        F.qsales (bwd oid) (bwd sale) (bwd qty)
      | Classes(cclass) ->
        F.classes (bwd cclass)
      | Students(cclass,name) ->
        F.students (bwd cclass) (bwd name)
      | Tests(name,course,score) ->
        F.tests (bwd name) (bwd course) (bwd score)
      | Test(name,score) ->
        F.test (bwd name) (bwd score)
      | Student_res(key,score_avg) ->
        F.student_res (bwd key) (bwd score_avg)
      | Nested_res(cclass,student) ->
        F.nested_res (bwd cclass) (bwd student)
      | Scores(cclass,score) ->
        F.scores (bwd cclass) (bwd score)
  end
  open X0
  module X = Trans_def(X0)
  open X
  module IDelta = struct
    (* records *)
    let product pid name price =
      Product(pid,name,price)
    let order oid pid qty =
      Order(oid,pid,qty)
    let sales pid name sale =
      Sales(pid,name,sale)
    let q_res key qty_sum =
      Q_res(key,qty_sum)
    let q1_res key sale_sum =
      Q1_res(key,sale_sum)
    let q2'_res key sale_sum qty_sum =
      Q2'_res(key,sale_sum,qty_sum)
    let q2_res key average =
      Q2_res(key,average)
    let q4_res key qty_avg =
      Q4_res(key,qty_avg)
    let q5_res key qty_count =
      Q5_res(key,qty_count)
    let q7_res key count_count = 
      Q7_res(key,count_count)
    let q8_res key sale_count = 
      Q8_res(key,sale_count)
    let q9_res key score_avg =
      Q9_res(key,score_avg)
    let osales oid sale =
      Osales (oid,sale)
    let qsales oid sale qty =
      Qsales (oid,sale,qty)
    let classes cclass = 
      Classes(cclass)
    let students cclass name = 
      Students(cclass,name)
    let tests name course score = 
      Tests(name,course,score)
    let test name score = 
      Test(name,score)
    let student_res key score_avg = 
      Student_res(key,score_avg)
    let nested_res cclass student = 
      Nested_res(cclass,student)
    let scores cclass score = 
      Scores(cclass,score)

    (* projection *)
    let pid = function
      | Product(pid,_,_) -> pid
      | x -> fwd @@ F.pid (bwd x)
    let name = function
      | Product(_,name,_) -> name
      | x -> fwd @@ F.name (bwd x)
    let price = function
      | Product(_,_,price) -> price
      | x -> fwd @@ F.price (bwd x)

    let oid = function
      | Order(oid,_,_) -> oid
      | x -> fwd @@ F.oid (bwd x)
    let opid = function
      | Order(_,pid,_) -> pid
      | x -> fwd @@ F.opid (bwd x)
    let qty = function
      | Order(_,_,qty) -> qty
      | x -> fwd @@ F.qty (bwd x)

    let spid = function
      | Sales(pid,_,_) -> pid
      | x -> fwd @@ F.spid (bwd x)
    let sname = function
      | Sales(_,name,_) -> name
      | x -> fwd @@ F.sname (bwd x)
    let sale = function
      | Sales(_,_,sale) -> sale
      | x -> fwd @@ F.sale (bwd x)

    let ooid = function
      | Osales(oid,_) -> oid
      | x -> fwd @@ F.ooid (bwd x)
    let osale = function
      | Osales(_,sale) -> sale
      | x -> fwd @@ F.osale (bwd x)

    let qoid = function
      | Qsales(oid,_,_) -> oid
      | x -> fwd @@ F.qoid (bwd x)
    let qsale = function
      | Qsales(_,sale,_) -> sale
      | x -> fwd @@ F.qsale (bwd x)
    let qqty = function
      | Qsales(_,_,qty) -> qty
      | x -> fwd @@ F.qqty (bwd x)

    let cclass = function
      | Classes(cclass) -> cclass
      | x -> fwd @@ F.cclass (bwd x)

    let stclass = function
      | Students(cclass,_) -> cclass
      | x -> fwd @@ F.stclass (bwd x)
    let stname = function
      | Students(_,name) -> name
      | x -> fwd @@ F.stname (bwd x)

    let tname = function
      | Tests(name,_,_) -> name
      | x -> fwd @@ F.tname (bwd x)
    let tcourse = function
      | Tests(_,course,_) -> course
      | x -> fwd @@ F.tcourse (bwd x)
    let tscore = function
      | Tests(_,_,score) -> score
      | x -> fwd @@ F.tscore (bwd x)

    let test_name = function
      | Test(name,_) -> name
      | x -> fwd @@ F.test_name (bwd x)
    let test_score = function
      | Test(_,score) -> score
      | x -> fwd @@ F.test_score (bwd x)

    let nested_class = function
      | Nested_res(cclass,_) -> cclass
      | x -> fwd @@ F.nested_class (bwd x)
    let nested_student = function
      | Nested_res(_,student) -> student
      | x -> fwd @@ F.nested_student (bwd x)

    let scores_class = function
      | Scores(cclass,_) -> cclass
      | x -> fwd @@ F.scores_class (bwd x)
    let scores_score = function
      | Scores(_,score) -> score
      | x -> fwd @@ F.scores_score (bwd x)

    let sale_sum    = fun o -> fmap F.sale_sum o
    let qty_avg     = fun o -> fmap F.qty_avg o
    let qty_count   = fun o -> fmap F.qty_count o
    let count_count = fun o -> fmap F.count_count o
    let sale_count  = fun o -> fmap F.sale_count o
    let qty_sum     = fun o -> fmap F.qty_sum o
    let score_avg   = fun o -> fmap F.score_avg o

    let q1_sale_sum = function
      | Q1_res(_,sale_sum) -> sale_sum
      | x -> fwd @@ F.q1_sale_sum (bwd x)
    let q2_key = function
      | Q2'_res(key,_,_) -> key
      | x -> fwd @@ F.q2_key (bwd x)
    let q2_sale_sum = function
      | Q2'_res(_,sale_sum,_) -> sale_sum
      | x -> fwd @@ F.q2_sale_sum (bwd x)
    let q2_qty_sum = function
      | Q2'_res(_,_,qty_sum) -> qty_sum
      | x -> fwd @@ F.q2_qty_sum (bwd x)
    let q3_sale_sum = function
      | Q1_res(_,sale_sum) -> sale_sum
      | x -> fwd @@ F.q3_sale_sum (bwd x)
    let q5_qty_count = function
      | Q5_res(_,qty_count) -> qty_count
      | x -> fwd @@ F.q5_qty_count (bwd x)
    let q7_count_count = function
      | Q7_res(_,count_count) -> count_count
      | x -> fwd @@ F.q7_count_count (bwd x)
    let q8_sale_count = function
      | Q8_res(_,sale_count) -> sale_count
      | x -> fwd @@ F.q8_sale_count (bwd x)
    let q9_score_avg = function
      | Student_res(_,score_avg) -> score_avg
      | x -> fwd @@ F.q9_score_avg (bwd x)

  end
end



module RecordBeta(F:SYM_SCHEMA) = struct
  module M = RecordBeta_pass(F)
  include O(M.X)(F)
  include M.IDelta
end

module ForYield(F:SYM_SCHEMA) = struct
  module M = Quelg_norm.ForYield_pass(F)
  include O(M.X)(F)
  include M.IDelta
end

module ForFor(F:SYM_SCHEMA) = struct
  module M = Quelg_norm.ForFor_pass(F)
  include O(M.X)(F)
  include M.IDelta
end

module ForWhere(F:SYM_SCHEMA) = struct
  module M = Quelg_norm.ForWhere_pass(F)
  include O(M.X)(F)
  include M.IDelta
end

module ForEmpty1(F:SYM_SCHEMA) = struct
  module M = Quelg_norm.ForEmpty1_pass(F)
  include O(M.X)(F)
  include M.IDelta
end

module ForUnionAll1(F:SYM_SCHEMA) = struct
  module M = Quelg_norm.ForUnionAll1_pass(F)
  include O(M.X)(F)
  include M.IDelta
end

module WhereTrue(F:SYM_SCHEMA) = struct
  module M = Quelg_norm.WhereTrue_pass(F)
  include O(M.X)(F)
  include M.IDelta
end

module WhereFalse(F:SYM_SCHEMA) = struct
  module M = Quelg_norm.WhereFalse_pass(F)
  include O(M.X)(F)
  include M.IDelta
end

(* the default optimizer of the schema that use dyn instead bwd.
   This is used in the normalization that analyze the body of lambda-abstraction. *)
module Dyn_schema(X:Trans_dyn)(F:SYM_SCHEMA with type 'a repr = 'a X.from) = struct
  open X
  open F

  (* records *)
  let product pid name price =
    fmap3 F.product pid name price
  let order oid pid qty =
    fmap3 F.order oid pid qty
  let sales pid name sale =
    fmap3 F.sales pid name sale
  let q_res key qty_sum =
    fmap2 F.q_res key qty_sum
  let q1_res key sale_sum =
    fmap2 F.q1_res key sale_sum
  let q2'_res key sale_sum qty_sum =
    fmap3 F.q2'_res key sale_sum qty_sum
  let q2_res key average =
    fmap2 F.q2_res key average
  let q4_res key qty_avg =
    fmap2 F.q4_res key qty_avg
  let q5_res key qty_count =
    fmap2 F.q5_res key qty_count
  let q7_res key count_count =
    fmap2 F.q7_res key count_count
  let q8_res key sale_count = 
    fmap2 F.q8_res key sale_count
  let q9_res key score_avg = 
    fmap2 F.q9_res key score_avg
  let osales oid sale =
    fmap2 F.osales oid sale
  let qsales oid sale qty =
    fmap3 F.qsales oid sale qty
  let classes cclass = 
    fmap F.classes cclass
  let students cclass name = 
    fmap2 F.students cclass name
  let tests name course score = 
    fmap3 F.tests name course score
  let test name score = 
    fmap2 F.test name score
  let student_res key score_avg = 
    fmap2 F.student_res  key score_avg
  let nested_res cclass student = 
    fmap2 F.nested_res cclass student
  let scores cclass score = 
    fmap2 F.scores cclass score

  (* projection *)
  let pid o      = fmap F.pid o
  let name o     = fmap F.name o
  let price o    = fmap F.price o

  let oid o   = fmap F.oid o
  let opid o  = fmap F.opid o
  let qty o   = fmap F.qty o

  let spid o  = fmap F.spid o
  let sname o = fmap F.sname o
  let sale o  = fmap F.sale o

  let ooid o  = fmap F.ooid o
  let osale o = fmap F.osale o

  let qoid o   = fmap F.qoid o
  let qsale o  = fmap F.qsale o
  let qqty o   = fmap F.qqty o

  let cclass o = fmap F.cclass o

  let stclass o = fmap F.stclass o
  let stname o  = fmap F.stname o

  let tname o   = fmap F.tname o
  let tcourse o = fmap F.tcourse o
  let tscore o  = fmap F.tscore o

  let test_name o  = fmap F.test_name o
  let test_score o = fmap F.test_score o

  let nested_class o   = fmap F.nested_class o
  let nested_student o = fmap F.nested_student o

  let scores_class o = fmap F.scores_class o
  let scores_score o = fmap F.scores_score o

  let sale_sum o    = fmap F.sale_sum o
  let qty_avg o     = fmap F.qty_avg o
  let qty_count o   = fmap F.qty_count o
  let count_count o = fmap F.count_count o
  let sale_count o  = fmap F.sale_count o
  let qty_sum o     = fmap F.qty_sum o
  let score_avg o   = fmap F.score_avg o

  let q1_sale_sum o    = fmap F.q1_sale_sum o
  let q2_key o         = fmap F.q2_key o
  let q2_sale_sum o    = fmap F.q2_sale_sum o
  let q2_qty_sum o     = fmap F.q2_qty_sum o
  let q3_sale_sum o    = fmap F.q3_sale_sum o
  let q5_qty_count o   = fmap F.q5_qty_count o
  let q7_count_count o = fmap F.q7_count_count o
  let q8_sale_count o  = fmap F.q8_sale_count o
  let q9_score_avg o   = fmap F.q9_score_avg o

  let products = F.products
  let orders   = F.orders
  let table_classes = F.table_classes
  let table_students = F.table_students
  let table_tests = F.table_tests
end

module ForUnionAll2(F:SYM_SCHEMA) = struct
  module M = Quelg_norm.ForUnionAll2_pass(F)
  include OL(M.X)(F)
  include Dyn_schema(M.X)(F)
  include M.IDelta
end

module ForEmpty2(F:SYM_SCHEMA) = struct
  module M = Quelg_norm.ForEmpty2_pass(F)
  include OL(M.X)(F)
  include M.IDelta
  include Dyn_schema(M.X)(F)
end

module WhereEmpty(F:SYM_SCHEMA) = struct
  module M = Quelg_norm.WhereEmpty_pass(F)
  include O(M.X)(F)
  include M.IDelta
end

module WhereWhere(F:SYM_SCHEMA) = struct
  module M = Quelg_norm.WhereWhere_pass(F)
  include O(M.X)(F)
  include M.IDelta
end

module WhereFor(F:SYM_SCHEMA) = struct
  module M = Quelg_norm.WhereFor_pass(F)
  include O(M.X)(F)
  include M.IDelta
end

(* Composing transformations *)
module Norm(F:SYM_SCHEMA) = struct
  (* All normalization rules *)
  module AllPasses = AbsBeta(RecordBeta(ForYield(ForFor(ForWhere(ForEmpty1(ForUnionAll1(WhereTrue(WhereFalse(ForUnionAll2(ForEmpty2(WhereEmpty(WhereWhere(WhereFor(F))))))))))))))

  (* Mainly rules *)
  module MainPasses = AbsBeta(RecordBeta(ForFor(ForWhere(ForYield(WhereFor(WhereWhere(F)))))))
end

(* Repeating for the normalization *)
module rec FixP : (SYM_SCHEMA with type 'a obs = 'a P.obs) = struct
  module T = Norm(FixP)
  module TFix = T.MainPasses  (* T.AllPasses *)
  module S = P

  type 'a repr =
    {again: unit -> 'a TFix.repr;
     last:  unit -> 'a S.repr}
  type 'a obs  = 'a S.obs

  (* record *)
  let product pid name price =
    {again = (fun () -> TFix.product (pid.again ()) (name.again ()) (price.again ()));
     last  = (fun () -> S.product (pid.last ()) (name.last ()) (price.last ())); }

  let order oid pid qty =
    {again = (fun () -> TFix.order (oid.again ()) (pid.again ()) (qty.again ()));
     last  = (fun () -> S.order (oid.last ()) (pid.last ()) (qty.last ())); }

  let sales pid name sale =
    {again = (fun () -> TFix.sales (pid.again ()) (name.again ()) (sale.again ()));
     last  = (fun () -> S.sales (pid.last ()) (name.last ()) (sale.last ())); }

  let q_res key qty_sum =
    {again = (fun () -> TFix.q_res (key.again ()) (qty_sum.again ()));
     last  = (fun () -> S.q_res (key.last ()) (qty_sum.last ())); }

  let q1_res key sale_sum =
    {again = (fun () -> TFix.q1_res (key.again ()) (sale_sum.again ()));
     last  = (fun () -> S.q1_res (key.last ()) (sale_sum.last ())); }

  let q2'_res key sale_sum qty_sum =
    {again = (fun () -> TFix.q2'_res (key.again ()) (sale_sum.again ()) (qty_sum.again ()));
     last  = (fun () -> S.q2'_res (key.last ()) (sale_sum.last ()) (qty_sum.last ())); }

  let q2_res key average =
    {again = (fun () -> TFix.q2_res (key.again ()) (average.again ()));
     last  = (fun () -> S.q2_res (key.last ()) (average.last ())); }

  let q4_res key qty_avg =
    {again = (fun () -> TFix.q4_res (key.again ()) (qty_avg.again ()));
     last  = (fun () -> S.q4_res (key.last ()) (qty_avg.last ())); }

  let q5_res key qty_count =
    {again = (fun () -> TFix.q5_res (key.again ()) (qty_count.again ()));
     last  = (fun () -> S.q5_res (key.last ()) (qty_count.last ())); }

  let q7_res key count_count = 
    {again = (fun () -> TFix.q7_res (key.again ()) (count_count.again ()));
     last  = (fun () -> S.q7_res (key.last ()) (count_count.last ())); }

  let q8_res key sale_count = 
    {again = (fun () -> TFix.q8_res (key.again ()) (sale_count.again ()));
     last  = (fun () -> S.q8_res (key.last ()) (sale_count.last ())); }

  let q9_res key score_avg = 
    {again = (fun () -> TFix.q9_res (key.again ()) (score_avg.again ()));
     last  = (fun () -> S.q9_res (key.last ()) (score_avg.last ())); }

  let osales oid sale =
    {again = (fun () -> TFix.osales (oid.again ()) (sale.again ()));
     last  = (fun () -> S.osales (oid.last ()) (sale.last ())); }

  let qsales oid sale qty =
    {again = (fun () -> TFix.qsales (oid.again ()) (sale.again ()) (qty.again ()));
     last  = (fun () -> S.qsales (oid.last ()) (sale.last ()) (qty.last ())); }

  let classes cclass =
    {again = (fun () -> TFix.classes (cclass.again ()));
     last  = (fun () -> S.classes (cclass.last ())); }

  let students cclass name =
    {again = (fun () -> TFix.students (cclass.again ()) (name.again ()));
     last  = (fun () -> S.students (cclass.last ()) (name.last ())); }

  let tests name course score =
    {again = (fun () -> TFix.tests (name.again ()) (course.again ()) (score.again ()));
     last  = (fun () -> S.tests (name.last ()) (course.last ()) (score.last ())); }

  let test name score =
    {again = (fun () -> TFix.test (name.again ()) (score.again ()));
     last  = (fun () -> S.test (name.last ()) (score.last ())); }

  let student_res key score_avg =
    {again = (fun () -> TFix.student_res (key.again ()) (score_avg.again ()));
     last  = (fun () -> S.student_res (key.last ()) (score_avg.last ())); }

  let nested_res cclass student =
    {again = (fun () -> TFix.nested_res (cclass.again ()) (student.again ()));
     last  = (fun () -> S.nested_res (cclass.last ()) (student.last ())); }

  let scores cclass score =
    {again = (fun () -> TFix.scores (cclass.again ()) (score.again ()));
     last  = (fun () -> S.scores (cclass.last ()) (score.last ())); }

  (* projection *)
  let pid r =
    {again = (fun () -> TFix.pid (r.again ()));
     last  = (fun () -> S.pid (r.last ())); }
  let name r =
    {again = (fun () -> TFix.name (r.again ()));
     last  = (fun () -> S.name (r.last ())); }
  let price r =
    {again = (fun () -> TFix.price (r.again ()));
     last  = (fun () -> S.price (r.last ())); }

  let oid r =
    {again = (fun () -> TFix.oid (r.again ()));
     last  = (fun () -> S.oid (r.last ())); }
  let opid r =
    {again = (fun () -> TFix.opid (r.again ()));
     last  = (fun () -> S.opid (r.last ())); }
  let qty r =
    {again = (fun () -> TFix.qty (r.again ()));
     last  = (fun () -> S.qty (r.last ())); }

  let spid r =
    {again = (fun () -> TFix.spid (r.again ()));
     last  = (fun () -> S.spid (r.last ())); }
  let sname r =
    {again = (fun () -> TFix.sname (r.again ()));
     last  = (fun () -> S.sname (r.last ())); }
  let sale r =
    {again = (fun () -> TFix.sale (r.again ()));
     last  = (fun () -> S.sale (r.last ())); }

  let ooid r =
    {again = (fun () -> TFix.ooid (r.again ()));
     last  = (fun () -> S.ooid (r.last ())); }
  let osale r = 
    {again = (fun () -> TFix.osale (r.again ()));
     last  = (fun () -> S.osale (r.last ())); }

  let qoid r =
    {again = (fun () -> TFix.qoid (r.again ()));
     last  = (fun () -> S.qoid (r.last ())); }
  let qsale r = 
    {again = (fun () -> TFix.qsale (r.again ()));
     last  = (fun () -> S.qsale (r.last ())); }
  let qqty r =
    {again = (fun () -> TFix.qqty (r.again ()));
     last  = (fun () -> S.qqty (r.last ())); }

  let cclass r =
    {again = (fun () -> TFix.cclass (r.again ()));
     last  = (fun () -> S.cclass (r.last ())); }

  let stclass r =
    {again = (fun () -> TFix.stclass (r.again ()));
     last  = (fun () -> S.stclass (r.last ())); } 
  let stname r =
    {again = (fun () -> TFix.stname (r.again ()));
     last  = (fun () -> S.stname (r.last ())); }

  let tname r =
    {again = (fun () -> TFix.tname (r.again ()));
     last  = (fun () -> S.tname (r.last ())); }
  let tcourse r =
    {again = (fun () -> TFix.tcourse (r.again ()));
     last  = (fun () -> S.tcourse (r.last ())); }
  let tscore r =
    {again = (fun () -> TFix.tscore (r.again ()));
     last  = (fun () -> S.tscore (r.last ())); }

  let test_name r =
    {again = (fun () -> TFix.test_name (r.again ()));
     last  = (fun () -> S.test_name (r.last ())); }
  let test_score r =
    {again = (fun () -> TFix.test_score (r.again ()));
     last  = (fun () -> S.test_score (r.last ())); }

  let nested_class r =
    {again = (fun () -> TFix.nested_class (r.again ()));
     last  = (fun () -> S.nested_class (r.last ())); }
  let nested_student r =
    {again = (fun () -> TFix.nested_student (r.again ()));
     last  = (fun () -> S.nested_student (r.last ())); }

  let scores_class r =
    {again = (fun () -> TFix.scores_class (r.again ()));
     last  = (fun () -> S.scores_class (r.last ())); }
  let scores_score r =
    {again = (fun () -> TFix.scores_score (r.again ()));
     last  = (fun () -> S.scores_score (r.last ())); }

  let sale_sum r =
    {again = (fun () -> TFix.sale_sum (r.again ()));
     last  = (fun () -> S.sale_sum (r.last ())); }
  let qty_avg r =
    {again = (fun () -> TFix.qty_avg (r.again ()));
     last  = (fun () -> S.qty_avg (r.last ())); }
  let qty_count r =
    {again = (fun () -> TFix.qty_count (r.again ()));
     last  = (fun () -> S.qty_count (r.last ())); }
  let count_count r =
    {again = (fun () -> TFix.count_count (r.again ()));
     last  = (fun () -> S.count_count (r.last ())); }
  let sale_count r =
    {again = (fun () -> TFix.sale_count (r.again ()));
     last  = (fun () -> S.sale_count (r.last ())); }
  let qty_sum r =
    {again = (fun () -> TFix.qty_sum (r.again ()));
     last  = (fun () -> S.qty_sum (r.last ())); }
  let score_avg r =
    {again = (fun () -> TFix.score_avg (r.again ()));
     last  = (fun () -> S.score_avg (r.last ())); }

  let q1_sale_sum r =
    {again = (fun () -> TFix.q1_sale_sum (r.again ()));
     last  = (fun () -> S.q1_sale_sum (r.last ())); }
  let q2_key r =
    {again = (fun () -> TFix.q2_key (r.again ()));
     last  = (fun () -> S.q2_key (r.last ())); }
  let q2_sale_sum r =
    {again = (fun () -> TFix.q2_sale_sum (r.again ()));
     last  = (fun () -> S.q2_sale_sum (r.last ())); }
  let q2_qty_sum r =
    {again = (fun () -> TFix.q2_qty_sum (r.again ()));
     last  = (fun () -> S.q2_qty_sum (r.last ())); }
  let q3_sale_sum r = 
    {again = (fun () -> TFix.q3_sale_sum (r.again ()));
     last  = (fun () -> S.q3_sale_sum (r.last ())); }
  let q5_qty_count r =
    {again = (fun () -> TFix.q5_qty_count (r.again ()));
     last  = (fun () -> S.q5_qty_count (r.last ())); }
  let q7_count_count r =
    {again = (fun () -> TFix.q7_count_count (r.again ()));
     last  = (fun () -> S.q7_count_count (r.last ())); }
  let q8_sale_count r = 
    {again = (fun () -> TFix.q8_sale_count (r.again ()));
     last  = (fun () -> S.q8_sale_count (r.last ())); }
  let q9_score_avg r =
    {again = (fun () -> TFix.q9_score_avg (r.again ()));
     last  = (fun () -> S.q9_score_avg (r.last ())); }

  let products () = S.products ()
  let orders () = S.orders ()
  let table_classes () = S.table_classes ()
  let table_students () = S.table_students ()
  let table_tests () = S.table_tests ()

  let binop op1 op2 x y = {again = (fun () -> op1 (x.again ()) (y.again ()));
                           last  = (fun () -> op2 (x.last ()) (y.last ()));}

  let int    n = {again = (fun () -> TFix.int n);
                  last  = (fun () -> S.int n)}
  let float  f = {again = (fun () -> TFix.float f);
                  last  = (fun () -> S.float f)}
  let bool   b = {again = (fun () -> TFix.bool b);
                  last  = (fun () -> S.bool b)}
  let string s = {again = (fun () -> TFix.string s);
                  last  = (fun () -> S.string s)}

  let (+%)  x y  = binop (TFix.(+%)) (S.(+%)) x y
  let (-%)  x y  = binop (TFix.(-%)) (S.(-%)) x y
  let ( *%) x y  = binop (TFix.( *%)) (S.( *%)) x y
  let (/%)  x y  = binop (TFix.(/%)) (S.(/%)) x y

  let (+%.)  x y = binop (TFix.(+%.)) (S.(+%.)) x y
  let (-%.)  x y = binop (TFix.(-%.)) (S.(-%.)) x y
  let ( *%.) x y = binop (TFix.( *%.)) (S.( *%.)) x y
  let (/%.)  x y = binop (TFix.(/%.)) (S.(/%.)) x y

  let (>%)  x y  = binop (TFix.(>%)) (S.(>%)) x y
  let (<%)  x y  = binop (TFix.(<%)) (S.(<%)) x y
  let (=%)  x y  = binop (TFix.(=%)) (S.(=%)) x y

  let (>%.) x y  = binop (TFix.(>%.)) (S.(>%.)) x y
  let (<%.) x y  = binop (TFix.(<%.)) (S.(<%.)) x y
  let (=%.) x y  = binop (TFix.(=%.)) (S.(=%.)) x y

  let (>&)  x y  = binop (TFix.(>&)) (S.(>&)) x y
  let (<&)  x y  = binop (TFix.(<&)) (S.(<&)) x y
  let (=&)  x y  = binop (TFix.(=&)) (S.(=&)) x y

  let (>@)  x y  = binop (TFix.(>@)) (S.(>@)) x y
  let (<@)  x y  = binop (TFix.(<@)) (S.(<@)) x y
  let (=@)  x y  = binop (TFix.(=@)) (S.(=@)) x y

  let (!%)  x    = {
    again = (fun () -> TFix.(!%) (x.again ()));
    last  = (fun () -> S.(!%) (x.last ()))}

  let (&%)  x y  = binop (TFix.(&%)) (S.(&%)) x y
  let (|%)  x y  = binop (TFix.(|%)) (S.(|%)) x y

  let if_ b x y = {again = (fun () -> TFix.(if_) (b.again ())
                               (fun () -> (x ()).again ())
                               (fun () -> (y ()).again ()));
                   last  = (fun () -> S.(if_) (b.last ())
                               (fun () -> (x ()).last ())
                               (fun () -> (y ()).last ()))}

  let lam f = {
    again = (fun () ->
        TFix.lam (fun x ->
            let r = f {again = (fun () -> x);
                       last  = (fun () -> failwith "illegal call: lam 1")}
            in r.again ()));
    last  = (fun () ->
        S.lam (fun x ->
            let r = f {again = (fun () -> failwith "illegal call: lam 2");
                       last  = (fun () -> x)}
            in r.last ()));
  }

  let app x y = {
    again = (fun () -> TFix.app (x.again ()) (y.again ()));
    last  = (fun () -> S.app (x.last ()) (y.last ())) }


  let foreach src body = {
    again = (fun () -> TFix.foreach (fun () -> (src ()).again ()) (fun x ->
        let r = body {again = (fun () -> x);
                      last  = (fun () -> failwith "illegal call: foreach 1");}
            in r.again ()));
    last  = (fun () -> S.foreach (fun () -> (src ()).last ()) (fun x ->
        let r = body {again = (fun () -> failwith "illegal call: foreach 2");
                      last  = (fun () -> x);}
        in r.last ()));
  }

  let where test body = {
    again = (fun () -> TFix.where (test.again ()) (fun () ->
        let r = body ()
        in r.again ()));
    last  = (fun () -> S.where (test.last ()) (fun () ->
        let r = body ()
        in r.last ()))}

  let yield x = {
    again = (fun () -> TFix.yield (x.again ()));
    last  = (fun () -> S.yield (x.last ())); }

  let nil () = {
    again = (fun () -> TFix.nil ());
    last  = (fun () -> S.nil ()); }

  let exists x = {
    again = (fun () -> TFix.exists (x.again ()));
    last  = (fun () -> S.exists (x.last ())); }

  let (@%) x y = {
    again = (fun () -> TFix.(@%) (x.again ()) (y.again ()));
    last  = (fun () -> S.(@%) (x.last ()) (y.last ())); }

  let group key alpha query body =
    let rec aggr_pair res = function
      | [] -> res
      | (l,agg,ofield)::xs -> 
         let pair = (l,
                     {again = (fun () -> agg.again ());
                      last  = (fun () -> agg.last ())},
                     {again = (fun () -> ofield.again ());
                      last  = (fun () -> ofield.last ())})
         in aggr_pair (pair::res) xs
    in
    let rec aggr_again res = function
      | [] -> res
      | (l,agg,ofield)::xs -> aggr_again ((((fun x -> 
                                             let r = l {again = (fun () -> x); 
                                                        last  = (fun () -> failwith "illegal call: aggr 1")} 
                                             in r.again ()),agg.again (),ofield.again ()))::res) xs
    in 
    let rec aggr_last res = function
      | [] -> res
      | (l,agg,ofield)::xs -> aggr_last ((((fun x -> 
                                            let r = l {again = (fun () -> failwith "illegal call: aggr 2"); 
                                                       last  = (fun () -> x)} 
                                            in r.last ()),agg.last (),ofield.last ()))::res) xs
    in 
    {again = (fun () -> TFix.group (fun x -> 
                            let g = key {again = (fun () -> x); 
                                         last  = (fun () -> failwith "illegal call: aggr")} 
                            in g.again ()) (aggr_again [] (aggr_pair [] alpha)) (query.again ()) (fun x y ->
         let r1 = body {again = (fun () -> x);
                        last  = (fun () -> failwith "illegal call: group 1");}
         in 
         let r2 = r1 {again = (fun () -> y); 
                      last  = (fun () -> failwith "illegal call: group 1")} in r2.again ()));
     last  = (fun () -> S.group (fun x -> 
                            let g = key {again = (fun () -> failwith "illegal call: group 2"); 
                                         last  = (fun () -> x)} 
                            in g.last ()) (aggr_last [] (aggr_pair [] alpha)) (query.last ()) (fun x y ->
         let r1 = body {again = (fun () -> failwith "illegal call: group 2");
                        last  = (fun () -> x);}
         in 
         let r2 = r1 {again = (fun () -> failwith "illegal call group 2"); 
                      last  = (fun () -> y)} in r2.last ()));}

  let table (name,data) = {again = (fun () -> TFix.table (name,data));
                           last  = (fun () -> S.table (name,data))}

  let depth = ref 0
  let rec observe m =
    (* Printf.printf "[FixP] observe: depth %d\n" !depth; *)
    incr depth;
    let r = if !depth > 10
      then S.observe (fun () -> (m ()).last ())
      else TFix.observe (fun () -> (m ()).again ()) in
    decr depth;
    r

end

(* with normal-form checking *)
module rec FixGenSQL : (SYM_SCHEMA with type 'a obs = 'a GenSQL.obs) = struct
  module T = Norm(FixGenSQL)
  module TFix = T.MainPasses
  module S = GenSQL
  module NF = GenSQL (* checking normal form *)

  type 'a repr =
    {again: unit -> 'a TFix.repr;
     last:  unit -> 'a S.repr;
     form:  unit -> 'a NF.repr}
  type 'a obs  = 'a S.obs

  (* record *)
  let product pid name price =
    {again = (fun () -> TFix.product (pid.again ()) (name.again ()) (price.again ()));
     last  = (fun () -> S.product (pid.last ()) (name.last ()) (price.last ()));
     form  = (fun () -> NF.product (pid.form ()) (name.form ()) (price.form ())); }

  let order oid pid qty =
    {again = (fun () -> TFix.order (oid.again ()) (pid.again ()) (qty.again ()));
     last  = (fun () -> S.order (oid.last ()) (pid.last ()) (qty.last ()));
     form  = (fun () -> NF.order (oid.form ()) (pid.form ()) (qty.form ())); }

  let sales pid name sale =
    {again = (fun () -> TFix.sales (pid.again ()) (name.again ()) (sale.again ()));
     last  = (fun () -> S.sales (pid.last ()) (name.last ()) (sale.last ()));
     form  = (fun () -> NF.sales (pid.form ()) (name.form ()) (sale.form ())); }

  let q_res key qty_sum =
    {again = (fun () -> TFix.q_res (key.again ()) (qty_sum.again ()));
     last  = (fun () -> S.q_res (key.last ()) (qty_sum.last ()));
     form  = (fun () -> NF.q_res (key.form ()) (qty_sum.form ())); }

  let q1_res key sale_sum =
    {again = (fun () -> TFix.q1_res (key.again ()) (sale_sum.again ()));
     last  = (fun () -> S.q1_res (key.last ()) (sale_sum.last ()));
     form  = (fun () -> NF.q1_res (key.form ()) (sale_sum.form ())); }

  let q2'_res key sale_sum qty_sum =
    {again = (fun () -> TFix.q2'_res (key.again ()) (sale_sum.again ()) (qty_sum.again ()));
     last  = (fun () -> S.q2'_res (key.last ()) (sale_sum.last ()) (qty_sum.last ()));
     form  = (fun () -> NF.q2'_res (key.form ()) (sale_sum.form ()) (qty_sum.form ())); }

  let q2_res key average =
    {again = (fun () -> TFix.q2_res (key.again ()) (average.again ()));
     last  = (fun () -> S.q2_res (key.last ()) (average.last ()));
     form  = (fun () -> NF.q2_res (key.form ()) (average.form ())); }

  let q4_res key qty_avg =
    {again = (fun () -> TFix.q4_res (key.again ()) (qty_avg.again ()));
     last  = (fun () -> S.q4_res (key.last ()) (qty_avg.last ()));
     form  = (fun () -> NF.q4_res (key.form ()) (qty_avg.form ())); }

  let q5_res key qty_count =
    {again = (fun () -> TFix.q5_res (key.again ()) (qty_count.again ()));
     last  = (fun () -> S.q5_res (key.last ()) (qty_count.last ()));
     form  = (fun () -> NF.q5_res (key.form ()) (qty_count.form ())); }

  let q7_res key count_count =
    {again = (fun () -> TFix.q7_res (key.again ()) (count_count.again ()));
     last  = (fun () -> S.q7_res (key.last ()) (count_count.last ()));
     form  = (fun () -> NF.q7_res (key.form ()) (count_count.form ())); }

  let q8_res key sale_count = 
    {again = (fun () -> TFix.q8_res (key.again ()) (sale_count.again ()));
     last  = (fun () -> S.q8_res (key.last ()) (sale_count.last ()));
     form  = (fun () -> NF.q8_res (key.form ()) (sale_count.form ())); }

  let q9_res key score_avg = 
    {again = (fun () -> TFix.q9_res (key.again ()) (score_avg.again ()));
     last  = (fun () -> S.q9_res (key.last ()) (score_avg.last ()));
     form  = (fun () -> NF.q9_res (key.form ()) (score_avg.form ())); }

  let osales oid sale =
    {again = (fun () -> TFix.osales (oid.again ()) (sale.again ()));
     last  = (fun () -> S.osales (oid.last ()) (sale.last ()));
     form  = (fun () -> NF.osales (oid.form ()) (sale.form ())); }

  let qsales oid sale qty =
    {again = (fun () -> TFix.qsales (oid.again ()) (sale.again ()) (qty.again ()));
     last  = (fun () -> S.qsales (oid.last ()) (sale.last ()) (qty.last ()));
     form  = (fun () -> NF.qsales (oid.form ()) (sale.form ()) (qty.form ())); }

  let classes cclass =
    {again = (fun () -> TFix.classes (cclass.again ()));
     last  = (fun () -> S.classes (cclass.last ()));
     form  = (fun () -> NF.classes (cclass.form ())); }

  let students cclass name =
    {again = (fun () -> TFix.students (cclass.again ()) (name.again ()));
     last  = (fun () -> S.students (cclass.last ()) (name.last ()));
     form  = (fun () -> NF.students (cclass.form ()) (name.form ())); }

  let tests name course score =
    {again = (fun () -> TFix.tests (name.again ()) (course.again ()) (score.again ()));
     last  = (fun () -> S.tests (name.last ()) (course.last ()) (score.last ()));
     form  = (fun () -> NF.tests (name.form ()) (course.form ()) (score.form ())); }

  let test name score =
    {again = (fun () -> TFix.test (name.again ()) (score.again ()));
     last  = (fun () -> S.test (name.last ()) (score.last ()));
     form  = (fun () -> NF.test (name.form ()) (score.form ())); }

  let student_res key score_avg =
    {again = (fun () -> TFix.student_res (key.again ()) (score_avg.again ()));
     last  = (fun () -> S.student_res (key.last ()) (score_avg.last ()));
     form  = (fun () -> NF.student_res (key.form ()) (score_avg.form ())); }

  let nested_res cclass student =
    {again = (fun () -> TFix.nested_res (cclass.again ()) (student.again ()));
     last  = (fun () -> S.nested_res (cclass.last ()) (student.last ()));
     form  = (fun () -> NF.nested_res (cclass.form ()) (student.form ())); }

  let scores cclass score =
    {again = (fun () -> TFix.scores (cclass.again ()) (score.again ()));
     last  = (fun () -> S.scores (cclass.last ()) (score.last ()));
     form  = (fun () -> NF.scores (cclass.form ()) (score.form ())); }

  (* projection *)
  let pid r =
    {again = (fun () -> TFix.pid (r.again ()));
     last  = (fun () -> S.pid (r.last ()));
     form  = (fun () -> NF.pid (r.form ())); }
  let name r =
    {again = (fun () -> TFix.name (r.again ()));
     last  = (fun () -> S.name (r.last ()));
     form  = (fun () -> NF.name (r.form ())); }
  let price r =
    {again = (fun () -> TFix.price (r.again ()));
     last  = (fun () -> S.price (r.last ()));
     form  = (fun () -> NF.price (r.form ())); }

  let oid r =
    {again = (fun () -> TFix.oid (r.again ()));
     last  = (fun () -> S.oid (r.last ()));
     form  = (fun () -> NF.oid (r.form ())); }
  let opid r =
    {again = (fun () -> TFix.opid (r.again ()));
     last  = (fun () -> S.opid (r.last ()));
     form  = (fun () -> NF.opid (r.form ())); }
  let qty r =
    {again = (fun () -> TFix.qty (r.again ()));
     last  = (fun () -> S.qty (r.last ()));
     form  = (fun () -> NF.qty (r.form ())); }

  let spid r =
    {again = (fun () -> TFix.spid (r.again ()));
     last  = (fun () -> S.spid (r.last ()));
     form  = (fun () -> NF.spid (r.form ())); }
  let sname r =
    {again = (fun () -> TFix.sname (r.again ()));
     last  = (fun () -> S.sname (r.last ()));
     form  = (fun () -> NF.sname (r.form ())); }
  let sale r =
    {again = (fun () -> TFix.sale (r.again ()));
     last  = (fun () -> S.sale (r.last ()));
     form  = (fun () -> NF.sale (r.form ())); }

  let ooid r =
    {again = (fun () -> TFix.ooid (r.again ()));
     last  = (fun () -> S.ooid (r.last ())); 
     form  = (fun () -> NF.opid (r.form ())); }
  let osale r = 
    {again = (fun () -> TFix.osale (r.again ()));
     last  = (fun () -> S.osale (r.last ()));
     form  = (fun () -> NF.osale (r.form ())); }

  let qoid r =
    {again = (fun () -> TFix.qoid (r.again ()));
     last  = (fun () -> S.qoid (r.last ())); 
     form  = (fun () -> NF.qoid (r.form ())); }
  let qsale r = 
    {again = (fun () -> TFix.qsale (r.again ()));
     last  = (fun () -> S.qsale (r.last ()));
     form  = (fun () -> NF.qsale (r.form ())); }
  let qqty r =
    {again = (fun () -> TFix.qqty (r.again ()));
     last  = (fun () -> S.qqty (r.last ()));
     form  = (fun () -> NF.qqty (r.form ())); }

  let cclass r =
    {again = (fun () -> TFix.cclass (r.again ()));
     last  = (fun () -> S.cclass (r.last ()));
     form  = (fun () -> NF.cclass (r.form ())); }

  let stclass r =
    {again = (fun () -> TFix.stclass (r.again ()));
     last  = (fun () -> S.stclass (r.last ()));
     form  = (fun () -> NF.stclass (r.form ())); }
  let stname r =
    {again = (fun () -> TFix.stname (r.again ()));
     last  = (fun () -> S.stname (r.last ()));
     form  = (fun () -> NF.stname (r.form ())); }

  let tname r =
    {again = (fun () -> TFix.tname (r.again ()));
     last  = (fun () -> S.tname (r.last ()));
     form  = (fun () -> NF.tname (r.form ())); }
  let tcourse r =
    {again = (fun () -> TFix.tcourse (r.again ()));
     last  = (fun () -> S.tcourse (r.last ()));
     form  = (fun () -> NF.tcourse (r.form ())); }
  let tscore r =
    {again = (fun () -> TFix.tscore (r.again ()));
     last  = (fun () -> S.tscore (r.last ()));
     form  = (fun () -> NF.tscore (r.form ())); }

  let test_name r =
    {again = (fun () -> TFix.test_name (r.again ()));
     last  = (fun () -> S.test_name (r.last ()));
     form  = (fun () -> NF.test_name (r.form ())); }
  let test_score r =
    {again = (fun () -> TFix.test_score (r.again ()));
     last  = (fun () -> S.test_score (r.last ()));
     form  = (fun () -> NF.test_score (r.form ())); }

  let nested_class r =
    {again = (fun () -> TFix.nested_class (r.again ()));
     last  = (fun () -> S.nested_class (r.last ()));
     form  = (fun () -> NF.nested_class (r.form ())); }
  let nested_student r =
    {again = (fun () -> TFix.nested_student (r.again ()));
     last  = (fun () -> S.nested_student (r.last ()));
     form  = (fun () -> NF.nested_student (r.form ())); }

  let scores_class r =
    {again = (fun () -> TFix.scores_class (r.again ()));
     last  = (fun () -> S.scores_class (r.last ()));
     form  = (fun () -> NF.scores_class (r.form ())); }
  let scores_score r =
    {again = (fun () -> TFix.scores_score (r.again ()));
     last  = (fun () -> S.scores_score (r.last ()));
     form  = (fun () -> NF.scores_score (r.form ())); }

  let sale_sum r =
    {again = (fun () -> TFix.sale_sum (r.again ()));
     last  = (fun () -> S.sale_sum (r.last ()));
     form  = (fun () -> NF.sale_sum (r.form ())); }
  let qty_avg r =
    {again = (fun () -> TFix.qty_avg (r.again ()));
     last  = (fun () -> S.qty_avg (r.last ()));
     form  = (fun () -> NF.qty_avg (r.form ())); }
  let qty_count r =
    {again = (fun () -> TFix.qty_count (r.again ()));
     last  = (fun () -> S.qty_count (r.last ()));
     form  = (fun () -> NF.qty_count (r.form ())); }
  let count_count r =
    {again = (fun () -> TFix.count_count (r.again ()));
     last  = (fun () -> S.count_count (r.last ()));
     form  = (fun () -> NF.count_count (r.form ())); }
  let sale_count r = 
    {again = (fun () -> TFix.sale_count (r.again ()));
     last  = (fun () -> S.sale_count (r.last ()));
     form  = (fun () -> NF.sale_count (r.form ())); }
  let qty_sum r =
    {again = (fun () -> TFix.qty_sum (r.again ()));
     last  = (fun () -> S.qty_sum (r.last ()));
     form  = (fun () -> NF.qty_sum (r.form ())); }
  let score_avg r =
    {again = (fun () -> TFix.score_avg (r.again ()));
     last  = (fun () -> S.score_avg (r.last ()));
     form  = (fun () -> NF.score_avg (r.form ())); }

  let q1_sale_sum r =
    {again = (fun () -> TFix.q1_sale_sum (r.again ()));
     last  = (fun () -> S.q1_sale_sum (r.last ()));
     form  = (fun () -> NF.q1_sale_sum (r.form ())); }
  let q2_key r =
    {again = (fun () -> TFix.q2_key (r.again ()));
     last  = (fun () -> S.q2_key (r.last ()));
     form  = (fun () -> NF.q2_key (r.form ())); }
  let q2_sale_sum r =
    {again = (fun () -> TFix.q2_sale_sum (r.again ()));
     last  = (fun () -> S.q2_sale_sum (r.last ()));
     form  = (fun () -> NF.q2_sale_sum (r.form ())); }
  let q2_qty_sum r =
    {again = (fun () -> TFix.q2_qty_sum (r.again ()));
     last  = (fun () -> S.q2_qty_sum (r.last ()));
     form  = (fun () -> NF.q2_qty_sum (r.form ())); }
  let q3_sale_sum r =
    {again = (fun () -> TFix.q3_sale_sum (r.again ()));
     last  = (fun () -> S.q3_sale_sum (r.last ()));
     form  = (fun () -> NF.q3_sale_sum (r.form ())); }
  let q5_qty_count r =
    {again = (fun () -> TFix.q5_qty_count (r.again ()));
     last  = (fun () -> S.q5_qty_count (r.last ()));
     form  = (fun () -> NF.q5_qty_count (r.form ())); }
  let q7_count_count r =
    {again = (fun () -> TFix.q7_count_count (r.again ()));
     last  = (fun () -> S.q7_count_count (r.last ()));
     form  = (fun () -> NF.q7_count_count (r.form ())); }
  let q8_sale_count r =
    {again = (fun () -> TFix.q8_sale_count (r.again ()));
     last  = (fun () -> S.q8_sale_count (r.last ()));
     form  = (fun () -> NF.q8_sale_count (r.form ())); }
  let q9_score_avg r =
    {again = (fun () -> TFix.q9_score_avg (r.again ()));
     last  = (fun () -> S.q9_score_avg (r.last ()));
     form  = (fun () -> NF.q9_score_avg (r.form ())); }

  (* data sources *)
  let products () = S.products ()
  let orders () = S.orders ()
  let table_classes () = S.table_classes ()
  let table_students () = S.table_students ()
  let table_tests () = S.table_tests ()


  (* Symantics constructs *)
  let unop op1 op2 op3 x =
    {again = (fun () -> op1 (x.again ()));
     last  = (fun () -> op2 (x.last ()));
     form  = (fun () -> op3 (x.form ())); }

  let binop op1 op2 op3 x y =
    {again = (fun () -> op1 (x.again ()) (y.again ()));
     last  = (fun () -> op2 (x.last ()) (y.last ()));
     form  = (fun () -> op3 (x.form ()) (y.form ()));}

  let const op1 op2 op3 c =
    {again = (fun () -> op1 c);
     last  = (fun () -> op2 c);
     form  = (fun () -> op3 c); }

  let int    n = const TFix.int S.int NF.int n
  let float  f = const TFix.float S.float NF.float f
  let bool   b = const TFix.bool S.bool NF.bool b
  let string s = const TFix.string S.string NF.string s

  let (+%)  x y  = binop (TFix.(+%)) (S.(+%)) (NF.(+%)) x y
  let (-%)  x y  = binop (TFix.(-%)) (S.(-%)) (NF.(-%)) x y
  let ( *%) x y  = binop (TFix.( *%)) (S.( *%)) (NF.( *%)) x y
  let (/%)  x y  = binop (TFix.(/%)) (S.(/%)) (NF.(/%)) x y

  let (+%.)  x y = binop (TFix.(+%.)) (S.(+%.)) (NF.(+%.))  x y
  let (-%.)  x y = binop (TFix.(-%.)) (S.(-%.)) (NF.(-%.)) x y
  let ( *%.) x y = binop (TFix.( *%.)) (S.( *%.)) (NF.( *%.)) x y
  let (/%.)  x y = binop (TFix.(/%.)) (S.(/%.)) (NF.(/%.))x y

  let (>%)  x y  = binop (TFix.(>%)) (S.(>%)) (NF.(>%)) x y
  let (<%)  x y  = binop (TFix.(<%)) (S.(<%)) (NF.(<%)) x y
  let (=%)  x y  = binop (TFix.(=%)) (S.(=%)) (NF.(=%)) x y

  let (>%.) x y  = binop (TFix.(>%.)) (S.(>%.)) (NF.(>%.)) x y
  let (<%.) x y  = binop (TFix.(<%.)) (S.(<%.)) (NF.(<%.)) x y
  let (=%.) x y  = binop (TFix.(=%.)) (S.(=%.)) (NF.(=%.)) x y

  let (>&)  x y  = binop (TFix.(>&)) (S.(>&)) (NF.(>&)) x y
  let (<&)  x y  = binop (TFix.(<&)) (S.(<&)) (NF.(<&)) x y
  let (=&)  x y  = binop (TFix.(=&)) (S.(=&)) (NF.(=&)) x y

  let (>@)  x y  = binop (TFix.(>@)) (S.(>@)) (NF.(>@)) x y
  let (<@)  x y  = binop (TFix.(<@)) (S.(<@)) (NF.(<@)) x y
  let (=@)  x y  = binop (TFix.(=@)) (S.(=@)) (NF.(=@)) x y

  let (!%)  x    = unop (TFix.(!%)) (S.(!%)) (NF.(!%)) x

  let (&%)  x y  = binop (TFix.(&%)) (S.(&%)) (NF.(&%)) x y
  let (|%)  x y  = binop (TFix.(|%)) (S.(|%)) (NF.(|%)) x y

  let if_ b x y = {again = (fun () -> TFix.(if_) (b.again ())
                               (fun () -> (x ()).again ())
                               (fun () -> (y ()).again ()));
                   last  = (fun () -> S.(if_) (b.last ())
                               (fun () -> (x ()).last ())
                               (fun () -> (y ()).last ()));
                   form  = (fun () -> NF.(if_) (b.form ())
                               (fun () -> (x ()).form ())
                               (fun () -> (y ()).form ())); }

  let lam f = {
    again = (fun () ->
        TFix.lam (fun x ->
            let r = f {again = (fun () -> x);
                       last  = (fun () -> failwith "illegal call: lam 1");
                       form  = (fun () -> failwith "illegal call: lam 1"); }
            in r.again ()));
    last  = (fun () ->
        S.lam (fun x ->
            let r = f {again = (fun () -> failwith "illegal call: lam 2");
                       last  = (fun () -> x);
                       form  = (fun () -> failwith "illegal call: lam 2"); }
            in r.last ()));
    form  = (fun () ->
        NF.lam (fun x ->
            let r = f {again = (fun () -> failwith "illegal call: lam 3");
                       last  = (fun () -> failwith "illegal call: lam 3");
                       form  = (fun () -> x); }
            in r.form ()));
  }

  let app e1 e2 = binop TFix.app S.app NF.app e1 e2

  let foreach src body = {
    again = (fun () -> TFix.foreach (fun () -> (src ()).again ()) (fun x ->
        let r = body {again = (fun () -> x);
                      last  = (fun () -> failwith "illegal call: foreach 1");
                      form  = (fun () -> failwith "illegal call: foreach 1"); }
            in r.again ()));
    last  = (fun () -> S.foreach (fun () -> (src ()).last ()) (fun x ->
        let r = body {again = (fun () -> failwith "illegal call: foreach 2");
                      last  = (fun () -> x);
                      form  = (fun () -> failwith "illegal call: foreach 3"); }
        in r.last ()));
    form  = (fun () -> NF.foreach (fun () -> (src ()).form ()) (fun x ->
        let r = body {again = (fun () -> failwith "illegal call: foreach 3");
                      last  = (fun () -> failwith "illegal call: foreach 3");
                      form  = (fun () -> x); }
        in r.form ()));
  }

  let where test body = {
    again = (fun () -> TFix.where (test.again ()) (fun () ->
        let r = body ()
        in r.again ()));
    last  = (fun () -> S.where (test.last ()) (fun () ->
        let r = body ()
        in r.last ()));
    form  = (fun () -> NF.where (test.form ()) (fun () ->
        let r = body ()
        in r.form ())); }

  let yield x = unop TFix.yield S.yield NF.yield x

  let nil () = {
    again = (fun () -> TFix.nil ());
    last  = (fun () -> S.nil ());
    form  = (fun () -> NF.nil ()); }

  let exists x = unop TFix.exists S.exists NF.exists x

  let (@%) x y = binop TFix.(@%) S.(@%) NF.(@%) x y

  let group key alpha query body =
    let rec aggr_pair res = function
      | [] -> res
      | (l,agg,ofield)::xs -> 
         let pair = (l,
                     {again = (fun () -> agg.again ());
                      last  = (fun () -> agg.last ());
                      form  = (fun () -> agg.form ())},
                     {again = (fun () -> ofield.again ());
                      last  = (fun () -> ofield.last ());
                      form  = (fun () -> ofield.form ())})
         in aggr_pair (pair::res) xs
    in
    let rec aggr_again res = function
      | [] -> res
      | (l,agg,ofield)::xs -> aggr_again ((((fun x -> 
                                             let r = l {again = (fun () -> x);
                                                        last  = (fun () -> failwith "illegal call: aggr 1");
                                                        form  = (fun () -> failwith "illegal call: aggr 1");} 
                                             in r.again ()),agg.again (),ofield.again ()))::res) xs
    in 
    let rec aggr_last res = function
      | [] -> res
      | (l,agg,ofield)::xs -> aggr_last ((((fun x -> 
                                            let r = l {again = (fun () -> failwith "illegal call: aggr 2"); 
                                                       last  = (fun () -> x);
                                                       form  = (fun () -> failwith "illegal call: aggr 3")} 
                                            in r.last ()),agg.last (),ofield.last ()))::res) xs
    in 
    let rec aggr_form res = function
      | [] -> res
      | (l,agg,ofield)::xs -> aggr_form ((((fun x -> 
                                            let r = l {again = (fun () -> failwith "illegal call: aggr 3"); 
                                                       last  = (fun () -> failwith "illegal call: aggr 3");
                                                       form  = (fun () -> x)} 
                                            in r.form ()),agg.form (),ofield.form ()))::res) xs

    in
    {again = (fun () -> TFix.group (fun x -> 
                            let g = key {again = (fun () -> x); 
                                         last  = (fun () -> failwith "illegal call: aggr"); 
                                         form  = (fun () -> failwith "illegal call: aggr")} 
                            in g.again ()) (aggr_again [] (aggr_pair [] alpha)) (query.again ()) (fun x y ->
         let r1 = body {again = (fun () -> x);
                        last  = (fun () -> failwith "illegal call: group 1");
                        form  = (fun () -> failwith "illegal call: group 1")}
         in 
         let r2 = r1 {again = (fun () -> y); 
                      last  = (fun () -> failwith "illegal call: group 1"); 
                      form  = (fun () -> failwith "illegal call: group 1")} in r2.again ()));
     last  = (fun () -> S.group (fun x -> 
                            let g = key {again = (fun () -> failwith "illegal call: group 1"); 
                                         last  = (fun () -> x); 
                                         form  = (fun () -> failwith "illegal call: group 1")} 
                            in g.last ()) (aggr_last [] (aggr_pair [] alpha)) (query.last ()) (fun x y ->
         let r1 = body {again = (fun () -> failwith "illegal call: group 2");
                        last  = (fun () -> x);
                        form  = (fun () -> failwith "illegal call: group 2")}
         in 
         let r2 = r1 {again = (fun () -> failwith "illegal call: group 2"); 
                      last  = (fun () -> y); 
                      form  = (fun () -> failwith "illegal call: group 2")} in r2.last ()));
     form = (fun () -> NF.group (fun x -> 
                           let g = key {again = (fun () -> failwith "illegal call: group 3"); 
                                        last  = (fun () -> failwith "illegal call: group 3"); 
                                        form  = (fun () -> x)} 
                           in g.form ()) (aggr_form [] (aggr_pair [] alpha)) (query.form ()) (fun x y ->
         let r1 = body {again = (fun () -> failwith "illegal call: group 3");
                        last  = (fun () -> failwith "illegal call: group 3");
                        form  = (fun () -> x)}
         in 
         let r2 = r1 {again = (fun () -> failwith "illegal call: group 3"); 
                      last  = (fun () -> failwith "illegal call: group 3"); 
                      form  = (fun () -> y)} in r2.form ()));}

  let table (name,data) = {again = (fun () -> TFix.table (name,data));
                           last  = (fun () -> S.table (name,data));
                           form  = (fun () -> NF.table (name,data)); }
  
  let depth = ref 0
  let rec observe m =
    (* Printf.printf "[FixGenSQL] observe: depth %d\n" !depth; *)
    incr depth;
    let r =
      if NF.check_NF (fun () -> (m ()).form ()) (* checking normal form *)
      then S.observe (fun () -> (m ()).last ())  (* generate an SQL query *)
      else if !depth > 100 then failwith "Normalization limit exceeded"
      else TFix.observe (fun () -> (m ()).again ()) (* normalize the term again *)
    in decr depth;
    r    
end
