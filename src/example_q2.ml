(* 
 * Examples of Q_2
 * group is used in query's input => subquery
 *
 * Q_2' = G(oid,α)(for(p <- table("products"))
 *                  for(o <- table("orders"))
 *                  where(p.pid = o.pid)
 *                  yield {oid = o.oid, sale = p.price * o.qty, qty = o.qty})
 *  Where α = {(sale, SUM, sale_sum), (qty, SUM, qty_sum)}
 *
 * Q_2 = for(q <- Q_2')
 *       yield {oid = q.oid, average = q.sale_sum/q.qty_sum}
 *
 *)

#load "quel.cma";;
#load "schema.cma";;

open Schema
open Printf

module Q2'(S:SYM_SCHEMA) = struct
  open S
  let table_orders   = table ("orders", orders ())
  let table_products = table ("products", products ())

  let key = qoid
  let alpha = [(qsale, (string "sum"), (string "sale_sum"));
               (qqty,  (string "sum"), (string "qty_sum"))]
  let q2' = group key alpha (foreach (fun () -> table_products) @@ fun p ->
                             foreach (fun () -> table_orders) @@ fun o ->
                             where ((pid p) =% (opid o)) @@ fun () ->
                             yield @@ qsales (oid o) ((price p) *% (qty o)) (qty o))
                  (fun v key -> q2'_res key (sale_sum v) (qty_sum v))

  let observe = observe
end

module Q2(S:SYM_SCHEMA) = struct
  open S
  module M = Q2'(S)

  let q2 = foreach (fun () -> M.q2') @@ fun q ->
           yield @@ q2_res (q2_key q) ((q2_sale_sum q) /% (q2_qty_sum q))

  let observe = observe
end

(* Auxiliary printers *)
let rec print_average = function
  | [] -> ""
  | (o::os) -> "<key=" ^ (string_of_int o#key) ^
               ", average=" ^ (string_of_int o#average) ^
               ">; " ^ print_average os ;;

let module M = Q2(R) in
print_average @@ M.observe (fun () -> M.q2) ;;

let module M = Q2(FixP) in
print_endline @@ M.observe (fun () -> M.q2) ;;

let module M = Q2(FixGenSQL) in M.observe (fun () -> M.q2) ;;

(* Performance test *)
let avg l = (List.fold_left (+.) 0.0 l) /. float(List.length l) ;;

(* Normalize + SQL generation time *)
let r =
  let module M = Q2(FixGenSQL) in
  let perform () : float =
    let s = Sys.time () in
    ignore (M.observe (fun () -> M.q2));
    Sys.time () -. s
  in let rec loop (data:float list) = function
    | 0 -> data
    | n -> loop ((perform ())::data) (n-1)
  in loop [] 100
in avg r ;;
