(* 
 * Examples of Q_9
 * Query with Nested Data Structure
 *
 * nestedData = for(c <- table("classes"))
 *              yield {class = c.class; students =
 *                        G(name,α)(for(s <- table("students"))
 *                                   for(t <- table("tests"))
 *                                   where s.name = t.name
 *                                   yield {name = t.name, score = t.score})}
 *  Where α = {score, AVG, score_avg}
 *
 * Q_9 = G(class,α)(for(x <- nestedData)
 *                   for(y <- x.students)
 *                   yield{class = x.class, score = y.score})
 *   Where α = {score, AVG, score_avg}
 *
 *)


#load "quel.cma";;
#load "schema.cma";;

open Schema
open Printf

module NestedData(S:SYM_SCHEMA) = struct
  open S
  let table_classes = table ("classes", table_classes ())
  let table_students = table ("students", table_students ())
  let table_tests = table ("tests", table_tests ())

  let key = test_name
  let alpha = [(test_score, (string "avg"), (string "score_avg"))]

  let nestedData = foreach (fun () -> table_classes) @@ fun c ->
                   yield @@ nested_res (cclass c) (group key alpha (foreach (fun () -> table_students) @@ fun s ->
                                 foreach (fun () -> table_tests) @@ fun t ->
                                 where ((stname s) =@ (tname t)) @@ fun () ->
                                 yield @@ test (tname t) (tscore t)) (fun v key -> student_res key (score_avg v)))

  let observe = observe
end

module Q9(S:SYM_SCHEMA) = struct
  open S
  module M = NestedData(S)

  let key = scores_class
  let alpha = [(scores_score, (string "avg"), (string "score_avg"))]
  let q9 = group key alpha (foreach (fun () -> M.nestedData) @@ fun x ->
                            foreach (fun () -> (nested_student x)) @@ fun y ->
                            yield @@ scores (nested_class x) (q9_score_avg y)) 
                (fun v key -> q9_res key (score_avg v))

  let observe = observe
end

(* Auxiliary printers *)
let rec print_score_avg = function
  | [] -> ""
  | (o::os) -> "<key=" ^ (o#key) ^
               ", qty_count=" ^ (string_of_int o#score_avg) ^
               ">; " ^ print_score_avg os ;;

let module M = Q9(R) in
print_score_avg @@ M.observe (fun () -> M.q9) ;;

let module M = Q9(FixP) in
print_endline @@ M.observe (fun () -> M.q9) ;;

let module M = Q9(FixGenSQL) in M.observe (fun () -> M.q9) ;;

(* Performance test *)
let avg l = (List.fold_left (+.) 0.0 l) /. float(List.length l) ;;

(* Normalize + SQL generation time *)
let r =
  let module M = Q9(FixGenSQL) in
  let perform () : float =
    let s = Sys.time () in
    ignore (M.observe (fun () -> M.q9));
    Sys.time () -. s
  in let rec loop (data:float list) = function
    | 0 -> data
    | n -> loop ((perform ())::data) (n-1)
  in loop [] 100
in avg r ;;
