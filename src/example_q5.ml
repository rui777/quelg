(* 
 * Examples of Q_5
 * Query with Predicate
 *
 * Q_5' = λp. G(oid,α)(for(o <- table("orders"))
 *                       where p(o.qty)
 *                       yield o)
 *  Where α = {qty, COUNT, qty_count}
 *
 * Q_5 = Q_5' (λx. x > 2)
 *
 *)


#load "quel.cma";;
#load "schema.cma";;

open Schema
open Printf

module Q5'(S:SYM_SCHEMA) = struct
  open S
  let table_orders = table ("orders", orders ())
 
  let key = oid
  let alpha = [(qty, (string "count"), (string "qty_count"))]
  let q5' = fun p ->
       group key alpha
       (foreach (fun () -> table_orders) @@ fun o ->
        where (p (qty o)) @@ fun () ->
        yield o) @@ fun v key -> q5_res key (qty_count v)
end
 
 module Q5(S:SYM_SCHEMA) = struct
   open S
   module M = Q5'(S)
   let q5 = M.q5' (fun x -> x >% (int 2))
 
   let observe = observe
 end

(* Auxiliary printers *)
let rec print_qty_count = function
  | [] -> ""
  | (o::os) -> "<key=" ^ (string_of_int o#key) ^
               ", sale_sum=" ^ (string_of_int o#qty_count) ^
               ">; " ^ print_qty_count os ;;

let module M = Q5(R) in
print_qty_count @@ M.observe (fun () -> M.q5) ;;

let module M = Q5(FixP) in
print_endline @@ M.observe (fun () -> M.q5) ;;

let module M = Q5(FixGenSQL) in M.observe (fun () -> M.q5) ;;

(* Performance test *)
let avg l = (List.fold_left (+.) 0.0 l) /. float(List.length l) ;;

(* Normalize + SQL generation time *)
let r =
  let module M = Q5(FixGenSQL) in
  let perform () : float =
    let s = Sys.time () in
    ignore (M.observe (fun () -> M.q5));
    Sys.time () -. s
  in let rec loop (data:float list) = function
    | 0 -> data
    | n -> loop ((perform ())::data) (n-1)
  in loop [] 100
in avg r ;;
