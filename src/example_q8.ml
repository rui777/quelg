(* 
 * Examples of Q_8
 * double nested control structures(+ input normalization rules)
 *
 * Q_8' = G(sale_sum,α)(for(x <- Q3)
 *                       yield x)
 *  Where α = {sale_sum, COUNT, sale_count}
 *
 * Q_8 = for(g <- Q8')
 *       where g.sale_count = 1
 *       yield g
 *
 *)


#load "quel.cma";;
#load "schema.cma";;

open Schema
open Printf

module Q3'(S:SYM_SCHEMA) = struct
   open S
   let table_orders = table ("orders", orders ())
                    
   let q3' = lam (fun xoid ->
       foreach (fun () -> table_orders) @@ fun o ->
       where ((oid o) >% xoid) @@ fun () ->
       yield o)
end
 
module Q3''(S:SYM_SCHEMA) = struct
  open S
  let table_products = table ("products", products ())
 
  let q3'' = lam (fun o ->
       foreach (fun () -> table_products) @@ fun p ->
       where ((pid p) =% (opid o)) @@ fun () ->
       yield @@ osales (oid o) ((price p) *% (qty o)))
end
 
module Q3(S:SYM_SCHEMA) = struct
  open S
  module M1 = Q3'(S)
  module M2 = Q3''(S)
            
  let key = ooid
  let alpha = [(osale, (string "sum"), (string "sale_sum"))]
  let q3' = lam (fun x -> group key alpha
                              (foreach (fun () -> app M1.q3' x) @@ fun y ->
                               app M2.q3'' y) @@ fun v key -> q1_res key (sale_sum v))
 
  let q3 = app q3' (int 1)
 
  let observe = observe
end

module Q8'(S:SYM_SCHEMA) = struct
  open S
  module M = Q3(S)
           
  let key = q3_sale_sum
  let alpha = [(q3_sale_sum, (string "count"), (string "sale_count"))]
  let query = foreach (fun () -> M.q3) @@ fun x -> yield x

  let q8' = group key alpha query @@ fun v key -> q8_res key (sale_count v)

  let observe = observe
end

module Q8(S:SYM_SCHEMA) = struct
  open S
  module M = Q8'(S)

  let q8 = foreach (fun () -> M.q8') @@ fun g ->
           where ((q8_sale_count g) =% (int 1)) @@ fun () ->
           yield g

  let observe = observe
end

(* Auxiliary printers *)
let rec print_sale_count = function
  | [] -> ""
  | (o::os) -> "<key=" ^ (string_of_int o#key) ^
               ", sale_count=" ^ (string_of_int o#sale_count) ^
               ">; " ^ print_sale_count os ;;

let module M = Q8(R) in
print_sale_count @@ M.observe (fun () -> M.q8) ;;

let module M = Q8(FixP) in
print_endline @@ M.observe (fun () -> M.q8) ;;

let module M = Q8(FixGenSQL) in M.observe (fun () -> M.q8) ;;

(* Performance test *)
let avg l = (List.fold_left (+.) 0.0 l) /. float(List.length l) ;;

(* Normalize + SQL generation time *)
let r =
  let module M = Q8(FixGenSQL) in
  let perform () : float =
    let s = Sys.time () in
    ignore (M.observe (fun () -> M.q8));
    Sys.time () -. s
  in let rec loop (data:float list) = function
    | 0 -> data
    | n -> loop ((perform ())::data) (n-1)
  in loop [] 100
in avg r ;;
