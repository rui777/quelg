(* The R interpreter *)

open Quelg_sym

module R_base = struct
  type 'a repr = 'a

  let int n    = n
  let bool b   = b
  let string s = s

  let (+%)  = (+)
  let (-%)  = (-)
  let ( *%) = ( * )
  let (/%)  = (/)

  (* To use specific primitives, give type annotations *)
  let (>%) (e1:int repr) (e2:int repr) = e1 > e2
  let (<%) (e1:int repr) (e2:int repr) = e1 < e2
  let (=%) (e1:int repr) (e2:int repr) = e1 = e2

  let (>@) (e1:string repr) (e2:string repr) = e1 > e2
  let (<@) (e1:string repr) (e2:string repr) = e1 < e2
  let (=@) (e1:string repr) (e2:string repr) = e1 = e2

  let (>&) (e1:bool repr) (e2:bool repr) = e1 > e2
  let (<&) (e1:bool repr) (e2:bool repr) = e1 < e2
  let (=&) (e1:bool repr) (e2:bool repr) = e1 = e2

  let (!%)  = not
  let (&%) = (&&)
  let (|%) = (||)

  let if_ e e1 e2 = if e then e1 () else e2 ()

  let lam f = f
  let app e1 e2 = e1 e2

  type 'a obs = 'a repr
  let observe x = x ()
end

(* Float constructs *)
module R_float = struct
  include R_base

  let float f = f

  let (+%.)  = (+.)
  let (-%.)  = (-.)
  let (/%.)  = (/.)
  let ( *%.) = ( *.)

  let (>%.) (e1:float repr) (e2:float repr) = e1 > e2
  let (<%.) (e1:float repr) (e2:float repr) = e1 < e2
  let (=%.) (e1:float repr) (e2:float repr) = e1 = e2
end

module R = struct
  include R_float
end


(* The R interpreter with list and record *)
module RL = struct
  include R

  let rec foreach src body =
    match src () with
      [] -> []
    | (x::xs) -> (body x) @ (foreach (fun () -> xs) body)

  let where test body =
    if test then body () else []

  let yield x = [x]
  let nil ()  = []

  let exists = function
      [] -> false
    | _  -> true

  let (@%) e1 e2 = e1 @ e2

  (* sum in aggregate function *)
  let rec sum_results field query =
    match query with
    | [] -> 0
    | y::ys -> (field y) + (sum_results field ys)

  (* max in aggregate function *)
  let rec max_results field query =
    match query with
    | [] -> 0
    | y::ys -> max (field y) (max_results field ys)

  (* min in aggregate function *)
  let rec min_results field query =
    match query with
    | [] -> max_int
    | y::ys -> min (field y) (min_results field ys)

  (* count in aggregate function *)
  let count_results field query =
    let rec sub_count_results field query results =
    match query with
    | [] -> results
    | y::ys -> sub_count_results field ys (results + 1)
    in sub_count_results field query 0

  (* avg in aggregate function *)
  let avg_results field query =
    (sum_results field query) / (count_results field query)

  (* intermediate structure for using aggregate functions *)
  let field_agg = fun field value ->
    object
      method field = field
      method value = value
    end

  (* aggregate function *)
  let aggr alpha query key body =
    let rec sub_aggr alpha_ res =
      match alpha_ with
      | [] -> body res key
      | (l,agg,ofield)::xs ->
         begin
           match agg with
           | "sum" -> sub_aggr xs ((field_agg ofield (sum_results l query))::res)
           | "max" -> sub_aggr xs ((field_agg ofield (max_results l query))::res)
           | "min" -> sub_aggr xs ((field_agg ofield (min_results l query))::res)
           | "count" -> sub_aggr xs ((field_agg ofield (count_results l query))::res)
           | "avg" -> sub_aggr xs ((field_agg ofield (avg_results l query))::res)
           | _ -> failwith "error."
         end
    in sub_aggr alpha []

  let same_record key l1 l2 = (key l1) = (key l2)

  let rec grouping key acc = function
    | [] -> acc
    | hd::tl ->
       let l1,l2 = List.partition (same_record key hd) tl in
       grouping key ((hd::l1)::acc) l2

  let group key alpha query body =
    let g = grouping key [] query in
    let rec sub_group acc = function
      | [] -> acc
      | hd::tl -> sub_group ((aggr alpha hd (key (List.hd hd)) body)::acc) tl
    in sub_group [] g ;;

  let table (name, data) = data
end

