#use "topfind";;
#thread;;
#require "postgresql";;

open Printf
open Postgresql

let conn = new connection ~dbname:"testdb" ~host:"localhost" ~user:"ruiohkura" ~password:"user1" ()
let products = ["shirt"; "T-shirt"; "pants"; "suit"]
let query_products = "INSERT INTO products VALUES ($1,$2,$3)"

let show res =
  for tuple = 0 to res#ntuples - 1 do
    for field = 0 to res#nfields - 1 do
      printf "%s, " (res#getvalue tuple field)
    done;
    print_newline ()
  done

let pid_num = 10000
let rec insert_products num =
  let pid = string_of_int (pid_num - num) in
  let random_name = List.nth products (Random.int 3) in
  let random_price = string_of_int (Random.int 1000) in
  match num with
  | 0 -> ()
  | n -> show @@ conn#exec ~params:[|pid;random_name;random_price|] query_products;
         insert_products (n-1)

let _ = insert_products pid_num 
