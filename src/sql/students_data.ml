#use "topfind";;
#thread;;
#require "postgresql";;

open Printf
open Postgresql

let conn = new connection ~dbname:"testdb" ~host:"localhost" ~user:"ruiohkura" ~password:"user1" ()
let classes = ["A"; "B"; "C"; "D"; "E"]
let students = ["alan"; "bert"; "charlie"; "david"; "edward"; "alice"; "betty"; "clara"; "dora"; "eve"]
let query_students = "INSERT INTO students VALUES ($1,$2)"

let show res =
  for tuple = 0 to res#ntuples - 1 do
    for field = 0 to res#nfields - 1 do
      printf "%s, " (res#getvalue tuple field)
    done;
    print_newline ()
  done

let students_num = 10000
let rec insert_students num =
  let random_class = List.nth classes (Random.int 4) in
  let random_student = List.nth students (Random.int 9) in
  match num with
  | 0 -> ()
  | n -> show @@ conn#exec ~params:[|random_class;random_student|] query_students;
         insert_students (n-1)

let _ = insert_students students_num
