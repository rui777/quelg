#use "topfind";;
#thread;;
#require "postgresql";;

open Printf
open Postgresql

let conn = new connection ~dbname:"testdb" ~host:"localhost" ~user:"ruiohkura" ~password:"user1" ()
let classes = ["A"; "B"; "C"; "D"; "E"]
let query_classes = "INSERT INTO classes VALUES ($1)"

let show res =
  for tuple = 0 to res#ntuples - 1 do
    for field = 0 to res#nfields - 1 do
      printf "%s, " (res#getvalue tuple field)
    done;
    print_newline ()
  done

let rec insert_classes = function
  | [] -> ()
  | h::tl -> show @@ conn#exec ~params:[|h|] query_classes;
             insert_classes tl

let _ = insert_classes classes
