#use "topfind";;
#thread;;
#require "postgresql";;

open Printf
open Postgresql

let conn = new connection ~dbname:"testdb" ~host:"localhost" ~user:"ruiohkura" ~password:"user1" ()
let students = ["alan"; "bert"; "charlie"; "david"; "edward"; "alice"; "betty"; "clara"; "dora"; "eve"]
let courses = ["Math"; "Science"; "Physics"; "Biology"]
let query_tests = "INSERT INTO tests VALUES ($1,$2,$3)"

let show res =
  for tuple = 0 to res#ntuples - 1 do
    for field = 0 to res#nfields - 1 do
      printf "%s, " (res#getvalue tuple field)
    done;
    print_newline ()
  done

let tests_num = 10000
let rec insert_tests num =
  let random_student = List.nth students (Random.int 9) in
  let random_course = List.nth courses (Random.int 3) in
  let random_score = string_of_int (Random.int 100) in
  match num with
  | 0 -> ()
  | n -> show @@ conn#exec ~params:[|random_student;random_course;random_score|] query_tests;
         insert_tests (n-1)

let _ = insert_tests tests_num
