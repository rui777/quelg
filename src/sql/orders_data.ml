#use "topfind";;
#thread;;
#require "postgresql";;

open Printf
open Postgresql

let conn = new connection ~dbname:"testdb" ~host:"localhost" ~user:"ruiohkura" ~password:"user1" ()
let query_orders = "INSERT INTO orders VALUES ($1,$2,$3)"

let show res =
  for tuple = 0 to res#ntuples - 1 do
    for field = 0 to res#nfields - 1 do
      printf "%s, " (res#getvalue tuple field)
    done;
    print_newline ()
  done

let oid_num = 10000
let rec insert_orders num =
  let oid = string_of_int (pid_num - num + 1) in
  let random_pid = string_of_int (Random.int 10) in
  let random_qty = string_of_int (Random.int 50) in
  match num with
  | 0 -> ()
  | n -> show @@ conn#exec ~params:[|oid;random_pid;random_qty|] query_orders;
         insert_orders (n-1)

let _ = insert_orders oid_num 
