/* Setup Instruction for PostgreSQL

First, run a front-end of PostgreSQL
>psql

Create a database:
> create database example owner user1 encoding 'utf8';

`example` is database name, `user1` is the owner user of the database.

Change the database:
> \c example user1;

Run this file:
> \i schema1.sql

*/
 

/* setup and initialize tables */
drop table products;
drop table orders;

create table products(
    pid integer,
    name text,
    price integer
);

create table orders(
    oid integer,
    pid integer,
    qty integer
);

insert into products(pid, name, price)
values(110,'shirt',100);

insert into products(pid, name, price)
values(111,'T-shirt',200);

insert into products(pid, name, price)
values(210,'pants',500);

insert into products(pid, name, price)
values(310,'suit',1000);

insert into orders(oid, pid, qty)
values(1, 110, 2);

insert into orders(oid, pid, qty)
values(1, 111, 3);

insert into orders(oid, pid, qty)
values(2, 110, 5);

insert into orders(oid, pid, qty)
values(2, 210, 10);

insert into orders(oid, pid, qty)
values(2, 310, 15);

insert into orders(oid, pid, qty)
values(3, 310, 20);
