(* 
 * Examples of Q_7
 * double nested control structures
 *
 * Q_7' = G(qty_count,α)(for(x <- Q_5)
 *                        yield x)
 *  Where α = {qty_count, COUNT, count_count}
 *
 * Q_7 = for(g <- Q7')
 *       where g.count_count > 1
 *       yield g
 *
 *)


#load "quel.cma";;
#load "schema.cma";;

open Schema
open Printf

module Q5'(S:SYM_SCHEMA) = struct
  open S
  let table_orders = table ("orders", orders ())

  let key = oid
  let alpha = [(qty, (string "count"), (string "qty_count"))]
  let q5' = fun p ->
       group key alpha
       (foreach (fun () -> table_orders) @@ fun o ->
        where (p (qty o)) @@ fun () ->
        yield o) @@ fun v key -> q5_res key (qty_count v)

  let observe = observe
end

module Q5(S:SYM_SCHEMA) = struct
  open S
  module M = Q5'(S)
  let q5 = M.q5' (fun x -> x >% (int 2))

  let observe = observe
end

module Q7'(S:SYM_SCHEMA) = struct
  open S
  module M = Q5(S)

  let key = q5_qty_count
  let alpha = [(q5_qty_count, (string "count"), (string "count_count"))]
  let query = foreach (fun () -> M.q5) @@ fun x -> yield x

  let q7' = group key alpha query @@ fun v key -> q7_res key (count_count v)

  let observe = observe
end

module Q7(S:SYM_SCHEMA) = struct
  open S
  module M = Q7'(S)

  let q7 = foreach (fun () -> M.q7') @@ fun g ->
           where ((q7_count_count g) >% (int 1)) @@ fun () ->
           yield g

  let observe = observe
end

(* Auxiliary printers *)
let rec print_count_count = function
  | [] -> ""
  | (o::os) -> "<key=" ^ (string_of_int o#key) ^
               ", qty_count=" ^ (string_of_int o#count_count) ^
               ">; " ^ print_count_count os ;;

let module M = Q7(R) in
print_count_count @@ M.observe (fun () -> M.q7) ;;

let module M = Q7(FixP) in
print_endline @@ M.observe (fun () -> M.q7) ;;

let module M = Q7(FixGenSQL) in M.observe (fun () -> M.q7) ;;

(* Performance test *)
let avg l = (List.fold_left (+.) 0.0 l) /. float(List.length l) ;;

(* Normalize + SQL generation time *)
let r =
  let module M = Q7(FixGenSQL) in
  let perform () : float =
    let s = Sys.time () in
    ignore (M.observe (fun () -> M.q7));
    Sys.time () -. s
  in let rec loop (data:float list) = function
    | 0 -> data
    | n -> loop ((perform ())::data) (n-1)
  in loop [] 100
in avg r ;;
