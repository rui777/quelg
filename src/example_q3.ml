(* 
 * Examples of Q_3
 * composed Query
 * 
 * Q_3' = λoid. for(o <- table("orders"))
 *               where(o.oid = oid)
 *               yield o
 *
 * Q_3'' = λo. for(p <- table("products"))
 *              where(p.pid = o.pid)
 *              yield {oid = o.oid, sales = p.price * o.qty}
 *
 * Q_3 = λx. G(oid,α)(for(y <- Q_3' x) Q_3'' y) 1
 * 
 *)


#load "quel.cma";;
#load "schema.cma";;

open Schema
open Printf

module Q3'(S:SYM_SCHEMA) = struct
   open S
   let table_orders = table ("orders", orders ())
                    
   let q3' = lam (fun xoid ->
       foreach (fun () -> table_orders) @@ fun o ->
       where ((oid o) >% xoid) @@ fun () ->
       yield o)
end
 
module Q3''(S:SYM_SCHEMA) = struct
  open S
  let table_products = table ("products", products ())
 
  let q3'' = lam (fun o ->
       foreach (fun () -> table_products) @@ fun p ->
       where ((pid p) =% (opid o)) @@ fun () ->
       yield @@ osales (oid o) ((price p) *% (qty o)))
end
 
module Q3(S:SYM_SCHEMA) = struct
  open S
  module M1 = Q3'(S)
  module M2 = Q3''(S)
            
  let key = ooid
  let alpha = [(osale, (string "sum"), (string "sale_sum"))]
  let q3' = lam (fun x -> group key alpha
                              (foreach (fun () -> app M1.q3' x) @@ fun y ->
                               app M2.q3'' y) @@ fun v key -> q1_res key (sale_sum v))
 
  let q3 = app q3' (int 1)
 
  let observe = observe
end

(* Auxiliary printers *)
let rec print_sale_sum = function
  | [] -> ""
  | (o::os) -> "<key=" ^ (string_of_int o#key) ^
               ", sale_sum=" ^ (string_of_int o#sale_sum) ^
               ">; " ^ print_sale_sum os ;;

let module M = Q3(R) in
print_sale_sum @@ M.observe (fun () -> M.q3) ;;

let module M = Q3(FixP) in
print_endline @@ M.observe (fun () -> M.q3) ;;

let module M = Q3(FixGenSQL) in M.observe (fun () -> M.q3) ;;

(* Performance test *)
let avg l = (List.fold_left (+.) 0.0 l) /. float(List.length l) ;;

(* Normalize + SQL generation time *)
let r =
  let module M = Q3(FixGenSQL) in
  let perform () : float =
    let s = Sys.time () in
    ignore (M.observe (fun () -> M.q3));
    Sys.time () -. s
  in let rec loop (data:float list) = function
    | 0 -> data
    | n -> loop ((perform ())::data) (n-1)
  in loop [] 100
in avg r ;;
